# Cinema Context RDF: Converting the Cinema Context MySQL database to RDF

[![powered by](https://img.shields.io/badge/powered%20by-CREATE-red)](https://www.create.humanities.uva.nl/)
[![pipeline status](https://gitlab.com/uvacreate/cinema-context/cinema-context-rdf/badges/master/pipeline.svg)](https://gitlab.com/uvacreate/cinema-context/cinema-context-rdf/commits/master)

This repository contains scripts generating the Linked Open Data RDF version of [Cinema Context](http://www.cinemacontext.nl/). Examples of the resulting turtle can be found in the [documentation pages](https://uvacreate.gitlab.io/cinema-context/cinema-context-rdf/). The RDF can be queried in the [CREATE endpoint](https://data.create.humanities.uva.nl/).


## Data

### RDF
[![License: CC BY 4.0](https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/)

RDF data are licensed under a Creative Commons Attribution 4.0 International License (CC BY 4.0). See the [Obtaining the data](https://uvacreate.gitlab.io/cinema-context/cinema-context-rdf/gettingstarted/#obtaining-the-data) section on the Getting Started page to see where the data can be downloaded.

### Source code (scripts)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

The code in this respository can be (re)used under a MIT license.

### Documentation
[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/)

The [documentation pages](https://uvacreate.gitlab.io/cinema-context/cinema-context-rdf/) are licensed under a Creative Commons Attribution Share-Alike 4.0 International License (CC BY-SA 4.0).

## Citation

* Menno den Engelse, Leon van Wissen, Thunnis van Oort, and Julia Noordegraaf (2020): Cinema Context in RDF. Version 1.0. CREATE, University of Amsterdam. DANS. https://doi.org/10.17026/dans-z64-mrvb

```bibtex

@dataset{cinemacontextrdf2020v1,
  title        = {Cinema Context in RDF},
  author       = {Den Engelse, Menno and
                  Van Wissen, Leon and
                  Van Oort, Thunnis and
                  Noordegraaf, Julia},
  organization = {CREATE, University of Amsterdam},
  month        = {October},
  year         = {2020},
  publisher    = {DANS},
  version      = {1.0},
  doi          = {10.17026/dans-z64-mrvb},
  url          = {https://doi.org/10.17026/dans-z64-mrvb}
}

```

## Acknowledgement

* The project "Cinema Context LOUD: de geschiedenis van de Nederlandse filmcultuur beschikbaar als Linked Open Usable Data" was financed by DANS as a [‘Klein dataproject’](https://dans.knaw.nl/nl/actueel/nieuws/kdp-subsidie-toegekend-aan-10-nieuwe-projecten) (small data project), January 2020.

## Contact

Issues, suggestions, contributions can be entered in the [issue tracker](https://gitlab.com/uvacreate/cinema-context/cinema-context-rdf/-/issues).

For other questions, please contact [createlab@uva.nl](mailto:createlab@uva.nl).



