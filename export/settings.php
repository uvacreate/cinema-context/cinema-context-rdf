<?
$db = "cinemacontext";
$name = "root";
$pass = "cinemacontext";
$host = "db";
$port = "3306";	

$mysqli = @new mysqli($host, $name, $pass, $db, $port);
if ($mysqli->connect_error) {
    
    // Maybe the db is not yet initialized
    sleep(120);
    
    // Try again
    $mysqli = new mysqli($host, $name, $pass, $db, $port);
    if ($mysqli->connect_error) {

        die('Connect Error (' . $mysqli->connect_errno . ') '
                . $mysqli->connect_error);
    }
}

//printf("Initial character set: %s\n", $mysqli->character_set_name());

/* change character set to utf8 */
if (!$mysqli->set_charset("utf8")) {
    printf("Error loading character set utf8: %s\n", $mysqli->error);
    exit();
} else {
    //printf("Current character set: %s\n", $mysqli->character_set_name());
}

# This metadata is printed in a separate metadata file, 
# but can be echoed in other files if needed.
$metadata = '
{
<https://data.create.humanities.uva.nl/id/cinemacontext/> a schema:Dataset ;
    schema:alternateName "Cinema Context in RDF" ;
    schema:creator [ a schema:Organization ;
            schema:name "CREATE, An E-Humanities Perspective" ;
            schema:sameAs <http://ror.org/04dkp9463> ;
            schema:url <http://www.create.humanities.uva.nl/> ],
        [ a schema:Person ;
            schema:name "Menno den Engelse" ;
            schema:url <http://www.islandsofmeaning.nl/> ],
        [ a schema:Person ;
            schema:name "Leon van Wissen" ;
            schema:sameAs <http://orcid.org/0000-0001-8672-025X> ],
        [ a schema:Person ;
            schema:name "Thunnis van Oort" ;
            schema:sameAs <http://orcid.org/0000-0001-8912-0508> ],
        [ a schema:Person ;
            schema:name "Julia Noordegraaf" ;
            schema:sameAs <http://orcid.org/0000-0003-0146-642X> ] ;
    schema:dateModified "2020-10-29"^^xsd:date ;
    schema:datePublished "2020-10-29"^^xsd:date ;
    schema:description "Cinema Context (http://www.cinemacontext.nl/) is an online database containing places, persons and companies involved in more than 100,000 film screenings since 1895. It provides insight into the \'DNA\' of Dutch film and cinema culture. Thanks to a DANS grant for small data projects, the Cinema Context database has been converted into Linked Open Data. The editors of Cinema Context are located at the University of Amsterdam, within the CREATE Digital Humanities programme. A dump of the MySQL database is periodically deposited at DANS." ;
    schema:distribution [ a schema:DataDownload ;
            schema:contentUrl <https://gitlab.com/uvacreate/cinema-context/cinema-context-rdf/-/raw/v1.0/export/trig/cinema_context_in_rdf_trig_v1.0.zip> ;
            schema:encodingFormat "application/zip" ] ;
    schema:identifier <https://doi.org/10.17026/dans-z64-mrvb> ;
    schema:keywords "bioscoop, filmhuis, programma, voorstelling, theater, persoon, keuring, reisbioscoop"@nl,
        "cinema, film, movies, history, the netherlands, theater, theatre, venue, performance, programme, person, IMDB, censorship, exhibition, travelling cinema"@en ;
    schema:license <https://creativecommons.org/licenses/by/4.0/> ;
    schema:name "Cinema Context" ;
    schema:temporalCoverage "1890-01-01/.." ;
    schema:url <http://www.cinemacontext.nl/> ;
    schema:usageInfo <https://uvacreate.gitlab.io/cinema-context/cinema-context-rdf/> ;
    schema:version "1.0" .
}
'

?>
