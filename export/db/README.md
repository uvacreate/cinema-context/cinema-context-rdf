# Database files

If you use the docker-compose strategy to export the database to RDF, you can place you db dumps over here. Be sure to check the filenames in the `docker-compose.yml` file.

You can download the latest version of the Cinema Context database from DANS: https://doi.org/10.17026/dans-z9y-c5g6
