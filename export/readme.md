# Export scripts

For each entity, a script queries the MySQL database, converts the data to rdf and outputs it as a turtle file. Sometimes, most notably in [export-programmes.php](export-programmes.php), this is a rather timeconsuming process - largely due to a wondrous system of replacing ids with new ids and using them both.

## Data
A data dump can be downloaded from DANS Easy: 

* Dibbets, Dr. K. (Universiteit van Amsterdam) (2018): Cinema Context. Film in Nederland vanaf 1896: een encyclopedie van de filmcultuur. DANS. https://doi.org/10.17026/dans-z9y-c5g6

## How to run

### Option 1: php and own database server

Add the downloaded Cinema Context database dump to your own database server and edit `settings.php` with the correct paths, ports, and credentials. Execute `$ php filename.php > target.trig` in a command line per script to start the export procedure. 

### Option 2: running docker-compose up

Edit the `docker-compose.yml` file and make sure to mount the downloaded database dump to the entrypoint of the mysql container. Edit this part:

```yml
 volumes:
      - path/to/local/dump.mysql:/docker-entrypoint-initdb.d/dump.sql:ro
 ```

Once you execute `$ docker-compose up` in a command line, a database containing the Cinema Context dump will be created. Two minutes (sleep time, waiting for the import to finish, extend if necessary) later, the scripts will start outputting the data into RDF/Trig. 