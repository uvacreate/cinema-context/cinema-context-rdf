Data retrieved from https://query.wikidata.org/ using:

```SPARQL
SELECT * WHERE {

  {
    ?country wdt:P773 ?code ;
           rdfs:label ?nameNL, ?nameEN .
    OPTIONAL { ?country wdt:P1667 ?tgn }

  } UNION {
    ?country wdt:P298 ?code ;
             rdfs:label ?nameNL, ?nameEN .
    OPTIONAL { ?country wdt:P1667 ?tgn }
    }

    FILTER(LANG(?nameNL) = 'nl')
    FILTER(LANG(?nameEN) = 'en')

    FILTER(?country NOT IN (<http://www.wikidata.org/entity/Q29999>, <http://www.wikidata.org/entity/Q756617>, <http://www.wikidata.org/entity/Q838261>))

  }
```
