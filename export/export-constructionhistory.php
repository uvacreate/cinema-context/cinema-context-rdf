<?php 

include("settings.php");
include("functions.php");

$prefixes = "
@prefix rdfs:   <http://www.w3.org/2000/01/rdf-schema#> . 
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> . 
@prefix sem: <http://semanticweb.cs.vu.nl/2009/11/sem/> . 
@prefix owl: <http://www.w3.org/2002/07/owl#> . 
@prefix geo: <http://www.opengis.net/ont/geosparql#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> . 
@prefix schema: <http://schema.org/> . \n\n";

echo $prefixes;

echo "# named graph\n";
echo "<https://data.create.humanities.uva.nl/id/cinemacontext/> {\n\n";

echo "_:verbouwing a sem:EventType ;\n";
echo "\tschema:name \"Verbouwing\"@nl, \"Refurbishment\"@en . \n\n";

echo "_:nieuwbouw a sem:EventType ; \n";
echo "\tschema:name \"Nieuwbouw\"@nl, \"Construction\"@en . \n\n";

echo "_:verwoesting a sem:EventType ; \n";
echo "\tschema:name \"Verwoesting\"@nl, \"Destruction\"@en . \n\n";

$sql = "select h.*, i.new_id 
		from tblAddressConstructionHistory as h
        left join PersID as i on h.person_id = i.old_id";
$result = $mysqli->query($sql);

while ($row = $result->fetch_assoc()) {
    

    echo "<http://www.cinemacontext.nl/id/constructionevent/" . $row['address_id'] . "-" . $row['s_order'] . ">\n";

    echo "\tsem:hasPlace <http://www.cinemacontext.nl/id/place/" . $row['address_id'] . "> ;\n";

    if($row['construction_type'] == "Nieuwbouw") {
        echo "\tsem:eventType _:nieuwbouw ;\n";
    } elseif ($row['construction_type'] == "Verbouwing") {
        echo "\tsem:eventType _:verbouwing ;\n";
    } elseif ($row['construction_type'] == "Verwoesting") {
        echo "\tsem:eventType _:verwoesting ;\n";
    }

    if(!preg_match("/^[0-9]{4}(-([0-9]{2}|xx))?(-([0-9]{2}|xx))?$/", $row['construction_year']) && strlen($row['construction_year'])){
        $row['construction_year'] = substr($row['construction_year'],0,4);
    }
    $start = turtletime($row['construction_year'],$row['construction_year']);
    if(strlen($start)){
        echo $start;
    }

    if(strlen($row['person_id'])){
        echo "\tsem:hasActor [\n";
        echo "\t\trdf:value <http://www.cinemacontext.nl/id/P" . voorloopnullen($row['new_id']) . "> ;\n";
        echo "\t\tsem:roleType [\n";
        echo "\t\t\tschema:name \"architect\" ; \n";
        echo "\t\t\ta sem:RoleType ;\n";
        echo "\t\t] ;\n";
        echo "\t\ta sem:Role ;\n";
        echo "\t] ;\n";
    }
    
    if(strlen($row['wikidata_id_building'])){
        echo "\tsem:hasActor [\n";
        echo "\t\towl:sameAs <http://www.wikidata.org/entity/" . $row['wikidata_id_building'] . "> ;\n";
        echo "\t\ta sem:Object ;\n";
        echo "\t] ;\n";
    }

    if(strlen($row['info'])){
        echo "\tschema:description \"" . esc($row['info']) . "\" ;\n";
    }

    echo  "\ta sem:Event .\n\n";

}

// named graph end
echo "}\n";

