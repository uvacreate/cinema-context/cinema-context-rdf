# Overview



## Example queries

The Cinema Context website contains a number of [examples of research questions](http://www.cinemacontext.nl/cgi/b/bib/bib-idx?c=cccfilm;sid=8c3618fb3f2952faccc1774ba6e1c531;tpl=doehetzelf-vragen.tpl;sort=titel%20oplopend;q1=%2A;page=index;cc=cccfilm;lang=en) that can be applied to the data set, divided into simple and more complex questions. When the Cinema Context website was created, especially the more complex questions required the use of offline software (Microsoft Access). After conversion to RDF, this is no longer needed. 

Divided in three categories, basic, simple, and complex, we provide a number of SPARQL queries that address these example questions. Our goal is to deliver a number of standard queries for basic questions that can easily be adapted to specific research questions, and/or can be used as building blocks to create more complex queries. These standard queries facilitate the use of the data by researchers and other users and also stand as examples of the usefulness of the RDF format.

### 1. Basic questions

[See basic questions -->](./basic.md)

### 2. Simple questions

[See simple questions -->](./simple.md)

### 3. Complex questions

[See complex questions -->](./complex.md)

## Showcase

!!! Tip "Contributing"
    Have you found interesting entry points into the Cinema Context data? Do you know another clever way to query the data? Or can you do a federated query on you own dataset? Please let us know! Feel free to contribute to this page and the query [showcase](showcase/overview.md) by issuing a pull request in repository on our [GitLab](https://gitlab.com/uvacreate/cinema-context/cinema-context-rdf/) or by dropping us an [email](../../contact/).

