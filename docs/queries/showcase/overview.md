# Showcase

!!! Tip "Contributing"
    Have you found interesting entry points into the Cinema Context data? Do you know another clever way to query the data? Or can you do a federated query on you own dataset? Please let us know! Feel free to contribute to this page by issuing a pull request in repository on our [GitLab](https://gitlab.com/uvacreate/cinema-context/cinema-context-rdf/) or by dropping us an [email](../../../contact/).