### Simple questions
These 'simple questions' are the example questions available on Cinema Context (with only slight changes to the dates in order to somewhat widen the chronological scope).

## Queries
!!! Warning "Info"
    It could be that data has been added to the database and that the here presented counts and results therefore do not reflect the current state of the dataset.

#### 1. How many feature films were shown in Utrecht in 1946?

??? Example "Query"
    === "SPARQL"
    ```sparql
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX schema: <http://schema.org/>

    SELECT (COUNT(?screening) AS ?count) WHERE {
    
      GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {
        ?place a schema:Place ;
          schema:address/schema:addressLocality "Utrecht" .
        
        ?theater a schema:MovieTheater ;
          schema:location ?place .
        
        ?screening a schema:Event ;
          schema:location ?theater ;
          schema:startDate ?date .
        
        FILTER(YEAR(?date) = 1946)
      }            
    }
    ```

    Answer: 357

    | theaterName          | count |
    | -------------------- | ----- |
    | Palace               | 52    |
    | Vreeburg Bioscoop    | 52    |
    | Scala (Potterstraat) | 52    |
    | City                 | 52    |
    | Rembrandt            | 51    |
    | Flora                | 51    |
    | Olympia              | 47    |

#### 2. How many cinemas in Rotterdam screened films in 1931?

??? Example "Query"
    === "SPARQL"
    ```sparql
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX schema: <http://schema.org/>

    SELECT (COUNT (DISTINCT ?theater) AS ?count) WHERE {
    
      GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {
        ?place a schema:Place ;
          schema:address/schema:addressLocality "Rotterdam" .
        
        ?theater a schema:MovieTheater ;
          schema:location ?place .
        
        ?screening a schema:Event ;
          schema:location ?theater ;
          schema:startDate ?date .
        
        FILTER(YEAR(?date) = 1931)
      }            
    }
    ```

    Answer: 19

#### 3. What percentage of Amsterdam cinemas showed French films in 1928?

??? Example "Query"
    === "SPARQL"
    ```sparql
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX schema: <http://schema.org/>

    SELECT (?french_theaters / ?amsterdam_theaters * 100 AS ?percentage) WHERE {
      GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {

        {
          SELECT (COUNT(DISTINCT ?theater) AS ?amsterdam_theaters) WHERE {
            ?place a schema:Place ;
              schema:address/schema:addressLocality "Amsterdam" .

            ?theater a schema:MovieTheater ;
              schema:location ?place .
          }
        }  

        {
          SELECT (COUNT(DISTINCT ?theaterFR) AS ?french_theaters) WHERE {
            ?place a schema:Place ;
              schema:address/schema:addressLocality "Amsterdam" .

            ?theaterFR a schema:MovieTheater ;
              schema:location ?place .

            ?screening a schema:Event ;
              schema:location ?theaterFR ;
              schema:startDate ?date ; 
              schema:subEvent [ schema:inLanguage "F" ] .

            FILTER(YEAR(?date) = 1928)

          }
        }
      }        
    }
    ```

    Answer: 3.33%

#### 4. How many American feature films were banned by the board of censors?

For this query, you have to know that wether a film is banned or not is entered in the `schema:text` property on a `schema:Rating`. This field can have one of tese values: "toegelaten", "niet toegelaten". Keep in mind that we are looking for the number of films, and that a film might have been rated more than once.

??? Example "Query"
    === "SPARQL"
    ```sparql
    
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX schema: <http://schema.org/>

    SELECT (COUNT(DISTINCT ?film) AS ?count) WHERE {
      GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {
        
        ?film a schema:Movie ;
          schema:countryOfOrigin "USA" ;
          schema:contentRating ?rating .
        
        ?rating schema:text "niet toegelaten" . 
      } 
    }
    ```

    Answer: 315


#### 5. How many banned films were screened in 1930?

A banned film has a rating that contains a `schema:text "niet toegelaten"` property. The date is attached to the programme, but the film is presented in a sub event of this programme.

??? Example "Query"
    === "SPARQL"
    ```sparql
    
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX schema: <http://schema.org/>

    SELECT (COUNT(DISTINCT ?film) AS ?count) WHERE {
      GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {
        
        ?film a schema:Movie ;
          schema:contentRating ?rating .
        
        ?rating schema:text "niet toegelaten" . 
        
        ?programme a schema:Event ;
                schema:startDate ?date ;
                schema:subEvent [ schema:workPresented ?film ]
        
        FILTER(YEAR(?date) = 1930)
      } 
    }
    ```

    Answer: 38

#### 6. Where in Utrecht could one attend private screenings of the Netherlands Film League (Filmliga)?

Names for special film screenings (e.g. children's matinees, Christmas programme, and also 'Filmliga' programmes) are a schema:name property of the schema:Event that denotes a programme.  

??? Example "Query"
    === "SPARQL"
    ```sparql
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX schema: <http://schema.org/>
    
    SELECT ?nametheater (COUNT(?theater) AS ?nrofligascreenings) {
    
      GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {
    
        ?place a schema:Place ;
  		    schema:address/schema:addressLocality "Utrecht" .
    
        ?theater a schema:MovieTheater ;
  		    schema:location ?place ;
  		    schema:name ?nametheater .
    
        ?screening a schema:Event ;
  		    schema:location ?theater ;
            schema:name ?voorstellingsnaam .
    
        FILTER(CONTAINS(?voorstellingsnaam, "Filmliga"))
        # A case insensitive approach would be: FILTER(REGEX(?voorstellingsnaam, "Filmliga", "i"))
      }
    } GROUP BY ?theater ?nametheater ORDER BY DESC(?nrofligascreenings)   
    ```

    Result:

    | nametheater       | nrofligascreenings |
    | ----------------- | ------------------ |
    | Vreeburg Bioscoop | 42                 |
    | Rembrandt         | 5                  |
    | Olympia           | 1                  |




#### 7. When did Dutch music hall and cabaret artist Louis Davids start performing in cinemas? And when did he stop doing this?
#### 8. In which cities did the Salvation Army organise film shows in a travelling cinema?
#### 9. Which movie theatres were located in Amsterdam?

This query retrieves all the known movie theatres located in Amsterdam over the course of the entire period covered by Cinema Context. The results also list the street address and the URI of the cinema.

??? Example "Query"
    === "SPARQL"
    ```sparql
      PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
      PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
      PREFIX schema: <http://schema.org/>

      SELECT  ?nametheater ?address ?theater

      WHERE {
        GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {

              ?place a schema:Place ;
                schema:address/schema:addressLocality "Amsterdam" ;
                schema:address/schema:streetAddress ?address .

              ?theater a schema:MovieTheater ;
                schema:location ?place ;
                schema:name ?nametheater .
              
          }
        }        GROUP BY ?theater ?nametheater ORDER BY ASC(?nametheater)
    ```

    Results (Oct 2020)

    | nametheater                            | address                            | theater                                     |
    | -------------------------------------- | ---------------------------------- | ------------------------------------------- |
    | Alfa                                   | Hirschpassage 4                    | http://www.cinemacontext.nl/id/B000070      |
    | Alfa                                   | Kleine - Gartmanplantsoen 4        | http://www.cinemacontext.nl/id/B000070      |
    | Alhambra                               | Weteringschans 134                 | http://www.cinemacontext.nl/id/B000036      |
    | Ambassade                              | Jan Evertsenstraat 18 b            | http://www.cinemacontext.nl/id/B000037      |
    | Americain Bioscoop                     | Daniël Stalpertstraat 67           | http://www.cinemacontext.nl/id/B000071      |
    | Amstel                                 | Amstelstraat                       | http://www.cinemacontext.nl/id/B000038      |
    | Amsterdamsche Jeugdbioscoop            | Ceintuurbaan 338-340               | http://www.cinemacontext.nl/id/B000039      |
    | Apollo                                 | Haarlemmerdijk 82                  | http://www.cinemacontext.nl/id/B000047      |
    | Archipel                               | Zeeburgerstraat 6                  | http://www.cinemacontext.nl/id/B000040      |
    | Asta                                   | Rozengracht 117                    | http://www.cinemacontext.nl/id/B000041      |
    | Astoria                                | Mosplein 18                        | http://www.cinemacontext.nl/id/B000058      |
    | Bellevue Biosoop (Plantage Middenlaan) | Plantage Middenlaan 4a             | http://www.cinemacontext.nl/id/B001664      |
    | Bellevue Cinerama                      | Marnixstraat 400                   | http://www.cinemacontext.nl/id/B000042      |
    | Bellevue Leidsekade                    | Leidsekade 90                      | http://www.cinemacontext.nl/id/B000002      |
    | Beurs Bioscoop                         | Warmoesstraat 139                  | http://www.cinemacontext.nl/id/B000043      |
    | Bijou Biograph Theatre                 | Damstraat 20                       | http://www.cinemacontext.nl/id/B000083      |
    | Bio                                    | Middenweg 18-24                    | http://www.cinemacontext.nl/id/B000069      |
    | Bioscoop onbekend (Kerkstraat)         | Kerkstraat 311                     | http://www.cinemacontext.nl/id/B001956      |
    | Bioscoop onbekend 1 (Tuinstraat)       | Tuinstraat 144-146                 | http://www.cinemacontext.nl/id/B000074      |
    | Bioscoop onbekend 2                    | Goudsbloemstraat 52-54             | http://www.cinemacontext.nl/id/B000073      |
    | Café-Bioscoop Blaauw                   | Rembrandtplein 8                   | http://www.cinemacontext.nl/id/B000075      |
    | Calypso                                | Marnixstraat 402                   | http://www.cinemacontext.nl/id/B000080      |
    | Capitol (Damrak)                       | Damrak 64                          | http://www.cinemacontext.nl/id/B000091      |
    | Capitol (Rozengracht)                  | Rozengracht 117                    | http://www.cinemacontext.nl/id/B000035      |
    | Carré                                  | Amstel 115                         | http://www.cinemacontext.nl/id/B000044      |
    | Ceintuur Bioscoop                      | Ceintuurbaan 280                   | http://www.cinemacontext.nl/id/B000076      |
    | Ceintuur Theater                       | Ceintuurbaan 282-284               | http://www.cinemacontext.nl/id/B000005      |
    | Centraal (Nieuwendijk)                 | Nieuwendijk 67                     | http://www.cinemacontext.nl/id/B000045      |
    | Centraal Theater (Amstelstraat)        | Amstelstraat 14-18                 | http://www.cinemacontext.nl/id/B001519      |
    | Centrum                                | Lijnbaansgracht 31-32              | http://www.cinemacontext.nl/id/B001369      |
    | Cine D                                 | Plantage Middenlaan 4a             | http://www.cinemacontext.nl/id/B002212      |
    | Cineac Damrak                          | Damrak 64                          | http://www.cinemacontext.nl/id/B000006      |
    | Cineac Reguliersbreestraat             | Reguliersbreestraat 31             | http://www.cinemacontext.nl/id/B000034      |
    | Cinecenter                             | Lijnbaansgracht 236                | http://www.cinemacontext.nl/id/B000077      |
    | Cinema International                   | A. Allebéplein 4                   | http://www.cinemacontext.nl/id/B000046      |
    | Cinema Palace                          | Kalverstraat 224                   | http://www.cinemacontext.nl/id/B000078      |
    | Cinema Room                            | Da Costakade 74-102                | http://www.cinemacontext.nl/id/B000065      |
    | Cinema Royal                           | Nieuwendijk 154                    | http://www.cinemacontext.nl/id/B000025      |
    | Cinema West                            | A. Allebéplein 4                   | http://www.cinemacontext.nl/id/B000048      |
    | Cinema de la Monnaie                   | Kalverstraat 226                   | http://www.cinemacontext.nl/id/B000066      |
    | Cinetol                                | Tolstraat 160                      | http://www.cinemacontext.nl/id/B000007      |
    | Circus Arena                           | Hobbemakade, hoek Honthorststraat  | http://www.cinemacontext.nl/id/B001553      |
    | Circus Arena                           | Boerenwetering                     | http://www.cinemacontext.nl/id/B001553      |
    | City Bioscope                          | Prinsengracht 452                  | http://www.cinemacontext.nl/id/B000049      |
    | City Theater                           | Kleine-Gartmanplantsoen 13 - 17    | http://www.cinemacontext.nl/id/B000008      |
    | Climax Cinema                          | Rembrandtplein 19                  | http://www.cinemacontext.nl/id/B001422      |
    | Concertgebouw                          | Van Baerlestraat                   | http://www.cinemacontext.nl/id/B001550      |
    | Concertgebouw                          | Concertgebouwplein 2-6             | http://www.cinemacontext.nl/id/B001550      |
    | Concordia                              | Rapenburg 16 b                     | http://www.cinemacontext.nl/id/B001423      |
    | Corso                                  | Kalverstraat 122                   | http://www.cinemacontext.nl/id/B000009      |
    | Cultura                                | Tolstraat 160                      | http://www.cinemacontext.nl/id/B000050      |
    | Dam Bioscoop                           | Damstraat 23                       | http://www.cinemacontext.nl/id/B000051      |
    | Damstraat Bioscope                     | Damstraat 20                       | http://www.cinemacontext.nl/id/B000052      |
    | De Liefde                              | Da Costakade 74-102                | http://www.cinemacontext.nl/id/B000010      |
    | De Munt                                | Kalverstraat 226                   | http://www.cinemacontext.nl/id/B000068      |
    | De Uitkijk                             | Prinsengracht 452                  | http://www.cinemacontext.nl/id/B000031      |
    | De Vereeniging                         | Warmoesstraat 139                  | http://www.cinemacontext.nl/id/B001589      |
    | Desmet                                 | Plantage Middenlaan 4a             | http://www.cinemacontext.nl/id/B000011      |
    | Du Midi                                | Apollolaan 2                       | http://www.cinemacontext.nl/id/B000012      |
    | Edison (Elandsgracht)                  | Elandsgracht 92                    | http://www.cinemacontext.nl/id/B000013      |
    | Edison (Haarlemmerstraat)              | Haarlemmerstraat 12 a              | http://www.cinemacontext.nl/id/B001368      |
    | Eye Filmmuseum (IJpromenade)           | IJpromenade 1                      | http://www.cinemacontext.nl/id/B001689      |
    | FC Hyena                               | Aambeeldstraat 24                  | http://www.cinemacontext.nl/id/B001943      |
    | Familie Bioscoop                       | Nieuwendijk 11                     | http://www.cinemacontext.nl/id/B000053      |
    | Filmhallen                             | Hannie Dankbaarpassage 12          | http://www.cinemacontext.nl/id/B001696      |
    | Flora                                  | Amstelstraat 20-28                 | http://www.cinemacontext.nl/id/B000014      |
    | Goudsbloem                             | 2e Goudsbloemdwarsstraat           | http://www.cinemacontext.nl/id/B000054      |
    | Grand Théâtre                          | Amstelstraat 21-23                 | http://www.cinemacontext.nl/id/B000055      |
    | Haarlemmerpoort                        | Haarlemmerplein 33                 | http://www.cinemacontext.nl/id/B000056      |
    | Hallen                                 | Jan van Galenstraat 41             | http://www.cinemacontext.nl/id/B000015      |
    | Het Ketelhuis                          | Pazzanistraat 13                   | http://www.cinemacontext.nl/id/B001605      |
    | Holland Experience 3D Movie Theatre    | Jodenbreestraat 8-14               | http://www.cinemacontext.nl/id/B001597      |
    | Hollandia (Haarlemmerdijk)             | Haarlemmerdijk 161                 | http://www.cinemacontext.nl/id/B000059      |
    | Hollandia (Nieuwe Herengracht)         | Nieuwe Herengracht 117             | http://www.cinemacontext.nl/id/B000060      |
    | Huize Nieuw Vondel                     | Haarlemmerweg 201                  | http://www.cinemacontext.nl/id/B001957      |
    | Johannes Roothaan Huis                 | Rozengracht 133                    | http://www.cinemacontext.nl/id/B001532      |
    | Juliana                                | Nieuwendijk 29                     | http://www.cinemacontext.nl/id/B000061      |
    | Keur Bioscoop (Nieuwe Achtergracht)    | Nieuwe Achtergracht 140-144        | http://www.cinemacontext.nl/id/B001370      |
    | Keur Bioscoop (Warmoesstraat)          | Warmoesstraat 139                  | http://www.cinemacontext.nl/id/B000062      |
    | Koninklijk Instituut voor de Tropen    | Mauritskade 63                     | http://www.cinemacontext.nl/id/B000004      |
    | Kosmorama                              | Nieuwendijk 128-132                | http://www.cinemacontext.nl/id/B000063      |
    | Kriterion                              | Roeterstraat 34 (tot 1985)         | http://www.cinemacontext.nl/id/B000016      |
    | Kriterion                              | Roeterstraat 170                   | http://www.cinemacontext.nl/id/B000016      |
    | Leidseplein Theater                    | Leidseplein 12                     | http://www.cinemacontext.nl/id/B000064      |
    | Luxor                                  | Nieuwendijk 128-132                | http://www.cinemacontext.nl/id/B000017      |
    | Mille Colonnes                         | Rembrandtplein 11-15               | http://www.cinemacontext.nl/id/B001474      |
    | Mobile Theatre                         | IJsclubterrein                     | http://www.cinemacontext.nl/id/stand/002031 |
    | Mobile Theatre                         | Nieuwezijds Voorburgwal 323        | http://www.cinemacontext.nl/id/stand/002020 |
    | Mobile Theatre                         | Museumplein                        | http://www.cinemacontext.nl/id/stand/002031 |
    | Mobile Theatre                         | Amsteldijk, Landgoed De Meerhuizen | http://www.cinemacontext.nl/id/stand/002030 |
    | Mobile Theatre                         | stad                               | http://www.cinemacontext.nl/id/stand/002023 |
    | Nassau                                 | Lijnbaansgracht 31-32              | http://www.cinemacontext.nl/id/B000079      |
    | National Theater                       | Linnaeusstraat 20                  | http://www.cinemacontext.nl/id/B000081      |
    | Nederlands Filmmuseum (Potterstraat)   | Paulus Potterstraat 13             | http://www.cinemacontext.nl/id/B001588      |
    | Nederlands Filmmuseum (Vondelpark)     | Vondelpark 3                       | http://www.cinemacontext.nl/id/B001500      |
    | New York                               | Damstraat 20                       | http://www.cinemacontext.nl/id/B000082      |
    | Nova                                   | Prinsengracht 245 a                | http://www.cinemacontext.nl/id/B000019      |
    | Nöggerath                              | Reguliersbreestraat 34             | http://www.cinemacontext.nl/id/B000018      |
    | Odeon (Coehoornstraat)                 | Coehoornstraat 6                   | http://www.cinemacontext.nl/id/B000640      |
    | Odeon (Singel)                         | Singel 460                         | http://www.cinemacontext.nl/id/B000020      |
    | Odeon (Zeeburgerstraat)                | Zeeburgerstraat 6                  | http://www.cinemacontext.nl/id/B000100      |
    | Olympia (Bellamystraat)                | Bellamystraat 49                   | http://www.cinemacontext.nl/id/B000102      |
    | Olympia (Dapperstraat)                 | Dapperstraat 84                    | http://www.cinemacontext.nl/id/B000021      |
    | Olympia Paleis (Overtoom)              | Overtoom 135-137                   | http://www.cinemacontext.nl/id/B000101      |
    | Ons Genoegen                           | Haarlemmerplein 7                  | http://www.cinemacontext.nl/id/B001421      |
    | Ooster Theater                         | Middenweg 18-24                    | http://www.cinemacontext.nl/id/B000084      |
    | Oostersche Bioscoop                    | Weesperstraat 60-62                | http://www.cinemacontext.nl/id/B000085      |
    | Opera Bioscoop                         | Leidsekade 90                      | http://www.cinemacontext.nl/id/B001600      |
    | Paleis voor Volksvlijt                 | Frederiksplein                     | http://www.cinemacontext.nl/id/B000103      |
    | Panopticum (Amstelstraat)              | Amstelstraat 14-18                 | http://www.cinemacontext.nl/id/B000105      |
    | Panopticum (Nieuwendijk)               | Nieuwendijk                        | http://www.cinemacontext.nl/id/B000104      |
    | Parisien (Haarlemmerstraat)            | Haarlemmerstraat 31-39             | http://www.cinemacontext.nl/id/B001547      |
    | Parisien (Nieuwendijk)                 | Nieuwendijk 69                     | http://www.cinemacontext.nl/id/B000022      |
    | Passage                                | Nieuwendijk 186                    | http://www.cinemacontext.nl/id/B000023      |
    | Pathé                                  | Kalverstraat 122                   | http://www.cinemacontext.nl/id/B000106      |
    | Pathé Arena                            | Arena Boulevard                    | http://www.cinemacontext.nl/id/B001436      |
    | Pathé De Munt                          | Vijzelstraat 15                    | http://www.cinemacontext.nl/id/B001466      |
    | Plantage Bioscoop (Nieuwe Herengracht) | Nieuwe Herengracht 117             | http://www.cinemacontext.nl/id/B000107      |
    | Plaza                                  | Kalverstraat 226                   | http://www.cinemacontext.nl/id/B000024      |
    | Prinsen Theater                        | Prinsengracht 245 a                | http://www.cinemacontext.nl/id/B000086      |
    | Prinsesse Bioscoop                     | Haarlemmerdijk 161                 | http://www.cinemacontext.nl/id/B000087      |
    | R.K. Schoolbioscoop                    | Sint Willibrordusstraat 19         | http://www.cinemacontext.nl/id/B001257      |
    | Rembrandt Bioscope                     | Jodenbreestraat 23                 | http://www.cinemacontext.nl/id/B000108      |
    | Rembrandt Theater                      | Rembrandtplein 21-23               | http://www.cinemacontext.nl/id/B001249      |
    | Rembrandtplein Theater                 | Rembrandtplein 11-15               | http://www.cinemacontext.nl/id/B000109      |
    | Rex                                    | Haarlemmerstraat 31-39             | http://www.cinemacontext.nl/id/B000026      |
    | Rialto                                 | Ceintuurbaan 338-340               | http://www.cinemacontext.nl/id/B000027      |
    | Roode Bioscoop                         | Haarlemmerplein 7                  | http://www.cinemacontext.nl/id/B000088      |
    | Roxy                                   | Kalverstraat 224                   | http://www.cinemacontext.nl/id/B000028      |
    | Rozen Theater                          | Rozengracht 117                    | http://www.cinemacontext.nl/id/B000029      |
    | Rubens Bioscoop                        | Waterlooplein 80-82                | http://www.cinemacontext.nl/id/B000089      |
    | Saskia                                 | Rembrandtplein 19                  | http://www.cinemacontext.nl/id/B000090      |
    | Scala                                  | Haarlemmerstraat 31-39             | http://www.cinemacontext.nl/id/B000092      |
    | Smart Cinema                           | 1e Constantijn Huygensstraat 20    | http://www.cinemacontext.nl/id/B001544      |
    | Studio K (Roeterstraat)                | Roeterstraat 34 (tot 1985)         | http://www.cinemacontext.nl/id/B000110      |
    | Studio K (Roeterstraat)                | Roeterstraat 170                   | http://www.cinemacontext.nl/id/B000110      |
    | Studio K (Timorplein)                  | Timorplein 62                      | http://www.cinemacontext.nl/id/B001646      |
    | Swinden Theater                        | 1e Van Swindenstraat 72-76         | http://www.cinemacontext.nl/id/B000093      |
    | Tavenu                                 | Haarlemmerdijk 161                 | http://www.cinemacontext.nl/id/B000094      |
    | Thalia (A)                             | Haarlemmerplein 33                 | http://www.cinemacontext.nl/id/B000057      |
    | Thalia (B)                             | Nieuwendijk 29                     | http://www.cinemacontext.nl/id/B000095      |
    | Thalia (C)                             | Lijnbaansgracht 31-32              | http://www.cinemacontext.nl/id/B000099      |
    | Thalia (D)                             | Tolstraat 160                      | http://www.cinemacontext.nl/id/B000111      |
    | The Movies                             | Haarlemmerdijk 161                 | http://www.cinemacontext.nl/id/B000067      |
    | Tip-Top Theater                        | Jodenbreestraat 25-27              | http://www.cinemacontext.nl/id/B000096      |
    | Tivoli                                 | Reguliersbreestraat 26-28          | http://www.cinemacontext.nl/id/B000112      |
    | Tuschinski                             | Reguliersbreestraat 26-28          | http://www.cinemacontext.nl/id/B000030      |
    | Union                                  | Heiligeweg 34-36                   | http://www.cinemacontext.nl/id/B001242      |
    | Van Lennep Bioscoop                    | Kinkerstraat 309-311               | http://www.cinemacontext.nl/id/B001373      |
    | Victoria (Nieuwendijk)                 | Nieuwendijk 192                    | http://www.cinemacontext.nl/id/B000113      |
    | Victoria (Rapenburg)                   | Rapenburg 16 b                     | http://www.cinemacontext.nl/id/B001371      |
    | Victoria (Sloterkade)                  | Sloterkade 162                     | http://www.cinemacontext.nl/id/B000032      |
    | West End                               | Jan Evertsenstraat 18 b            | http://www.cinemacontext.nl/id/B000033      |
    | Wester Bioscoop                        | Haarlemmerplein 7                  | http://www.cinemacontext.nl/id/B000097      |
    | Witte Bioscoop                         | Damrak 64                          | http://www.cinemacontext.nl/id/B000098      |
    | Zonnehuis                              | Zonneplein 30                      | http://www.cinemacontext.nl/id/B000114      |
    | bioscoop onbekend 4                    | Goudsbloemstraat 86                | http://www.cinemacontext.nl/id/B001372      |

        
   