# Descriptive questions

These basic questions provide information on the composition of the database. See also the ['browse'](http://www.cinemacontext.nl/cgi/b/bib/bib-idx?c=cccfilm;sid=8c3618fb3f2952faccc1774ba6e1c531;tpl=browse.tpl;lang=en) function on Cinema Context.

## Queries
!!! Warning "Info"
    It could be that data has been added to the database and that the here presented counts and results therefore do not reflect the current state of the dataset.

#### 1. How many films/companies/persons/movie theaters are stored in the database? 

??? Example "How many films are in the dataset?"
    === "SPARQL"
    ```sparql
    
    PREFIX schema: <http://schema.org/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

    SELECT(COUNT(?film) AS ?numberoffilms) WHERE {
      GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {
        ?film a schema:Movie .
      }
    }
    ```
    
    Result: 51040 (2020-06-18)


??? Example "Total number of movie theaters (including mobile theaters)"
    === "SPARQL"
    ```sparql
    PREFIX schema: <http://schema.org/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    SELECT (COUNT(?movietheater) AS ?totalmovietheaters) WHERE {
      GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {
        ?movietheater a schema:MovieTheater.
      }
    } 
    ```
    
    Result: 1910 (2020-06-18)

??? Example "Generic count query for Movie, Organization, Person, and MovieTheater"
    === "SPARQL"
    ```sparql
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX schema: <http://schema.org/>

    SELECT ?type (COUNT(?resource) AS ?count) WHERE {
      GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {
        ?resource a ?type .
        
        VALUES ?type { schema:Movie schema:Organization schema:Person schema:MovieTheater }
        
      }
    } GROUP BY ?type ORDER BY ?type
    ```
    
    Result: (2020-09-24)

    | type                           | count |
    | ------------------------------ | ----- |
    | http://schema.org/Movie        | 51159 |
    | http://schema.org/MovieTheater | 1911  |
    | http://schema.org/Organization | 1641  |
    | http://schema.org/Person       | 4338  |

#### 2. Can we create a table that shows an overview of the number of films in the database ordered by year of production?

??? Example "Overview of the number of films in the database ordered by year of production"
    === "SPARQL"
    ``` sparql
    PREFIX schema: <http://schema.org/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    SELECT ?year_of_production (COUNT(?film) AS ?nrofilms) {
      GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {
        ?film a schema:Movie ;
                schema:dateCreated ?year_of_production.
      }
    } GROUP BY ?year_of_production ORDER BY ?year_of_production
    ```

    Result: table overview. 
    Note: total number of films with year of production = "21689"


#### 3. Can we create a list of all films without a known IMDb ID?

Here we use a query that counts all the films in the database, and then filters out the films for which the label 'schema:sameAs' (used in the model to indicate the films that _have_ an IMDb ID), does not exist.

??? Example "Number of films without link to IMDb"

    === "SPARQL"
    ```sparql
    PREFIX schema: <http://schema.org/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    SELECT (COUNT(?film) AS ?totalfilmsIMDbID) WHERE {
      GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {
      ?film a schema:Movie.
      FILTER NOT EXISTS {?film schema:sameAs ?IMDb}
      }
    } 
    ```

    Result: 33.273 (2020-06-18)


#### 4. Can we create an overview of the number of films per censorship rating category? Can we create lists for each category?

??? example "Number of films per censorship category"
    === "SPARQL"

    ```sparql
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX schema: <http://schema.org/>
    SELECT ?ratingtext (COUNT(DISTINCT ?film) AS ?nroffilms)  {
      GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {
        ?film a schema:Movie .
        
        OPTIONAL {?film schema:contentRating ?rating   .
        
        ?rating a schema:Rating ;
                        schema:text ?ratingtext .}
      }
    } GROUP BY ?ratingtext ORDER BY DESC(?nroffilms)
    ```
    Result: 

    | rating          | count                                               |
    | --------------- | --------------------------------------------------- |
    | [no rating]     | "23527"^^<http://www.w3.org/2001/XMLSchema#integer> |
    | alle leeftijden | "17551"^^<http://www.w3.org/2001/XMLSchema#integer> |
    | 18 jaar         | "6047"^^<http://www.w3.org/2001/XMLSchema#integer>  |
    | 14 jaar         | "5660"^^<http://www.w3.org/2001/XMLSchema#integer>  |
    | niet toegelaten | "755"^^<http://www.w3.org/2001/XMLSchema#integer>   |


  


#### 5. Can we create an alphabetical list of persons in the database, that includes year of birth and death?

Here we queried all the named persons in the database, adding the property surname to be able to list them alphabetically (using the baseSurname property to avoid ordering by suffix) and including birth date and death date as optional properties of the persons.

??? Example "Alphabetical list of persons in the database with year of birth and death"

    === "SPARQL"
    ```sparql
    PREFIX pnv: <https://w3id.org/pnv#>
    PREFIX schema: <http://schema.org/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    SELECT ?person ?surname ?birthdate ?deathdate WHERE {
      GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {
        ?person a schema:Person ; 
            pnv:hasName ?personname.
        
        ?personname a pnv:PersonName ;
            pnv:baseSurname ?surname.
            
        OPTIONAL {?person schema:birthDate ?birthdate}
        OPTIONAL {?person schema:deathDate ?deathdate}
      } 
    } ORDER BY ASC (LCASE(?surname))
    ```

    Result: 1,161 (2020-06-18)

    | person                                 | surname               | birthdate  | deathdate  |
    | -------------------------------------- | --------------------- | ---------- | ---------- |
    | http://www.cinemacontext.nl/id/P002292 | Aalst                 |            |            |
    | http://www.cinemacontext.nl/id/P001572 | Aardenburg-v.d. Wardt |            |            |
    | http://www.cinemacontext.nl/id/P002644 | Abeele                |            |            |
    | http://www.cinemacontext.nl/id/P001782 | Abeelen               |            |            |
    | http://www.cinemacontext.nl/id/P002024 | Alphen                |            |            |
    | http://www.cinemacontext.nl/id/P004010 | Alten                 |            |            |
    | http://www.cinemacontext.nl/id/P001118 | Amelsvoort            |            |            |
    | http://www.cinemacontext.nl/id/P002645 | Amelsvoort            |            |            |
    | http://www.cinemacontext.nl/id/P001763 | Amerongen             |            |            |
    | http://www.cinemacontext.nl/id/P002312 | Amerongen             |            |            |
    | http://www.cinemacontext.nl/id/P001437 | Amersfoort            |            |            |
    | http://www.cinemacontext.nl/id/P000199 | Ammers                |            |            |
    | http://www.cinemacontext.nl/id/P000618 | Appelen               |            |            |
    | http://www.cinemacontext.nl/id/P001970 | Arendonk              |            |            |
    | http://www.cinemacontext.nl/id/P000338 | Arendsen de Wolff     |            |            |
    | http://www.cinemacontext.nl/id/P002374 | Arent                 | 1884-04-22 | 1942-10-26 |
    | http://www.cinemacontext.nl/id/P000945 | Assem                 |            |            |
    | http://www.cinemacontext.nl/id/P003331 | Assem                 |            |            |
    | http://www.cinemacontext.nl/id/P000944 | Assem                 |            |            |
    | http://www.cinemacontext.nl/id/P003332 | Assenbergh            |            |            |
    | http://www.cinemacontext.nl/id/P004320 | Bakker                |            |            |
    | http://www.cinemacontext.nl/id/P001626 | Bakker                |            |            |
    | http://www.cinemacontext.nl/id/P003979 | Balbian Verster       | 1897       |            |


#### 6a. How many film screenings are stored in the database per year?

To find individual film screenings, we look for ScreeningEvents and convert the startDate to a year, and then group the result by years. This gives us all the individual film screenings; especially in the early period, many short films were screened per programme. If we want to know the number of programmes per year, we need another query, see 6b.

??? Example  "Amount of film screenings stored in the database per year"

    ```sparql 
    PREFIX schema: <http://schema.org/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    
    SELECT (COUNT(?filmscreening) AS ?nr_of_scr) ?year WHERE {
      GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {
            ?filmscreening a schema:ScreeningEvent .
        
        	?programme schema:subEvent ?filmscreening ;
        	           schema:startDate ?date .
          }
    } GROUP BY (YEAR(?date) AS ?year) ORDER BY (?year)
    ```
    
    Results (first 10 rows)
    
    | nr_of_scr                                         | year                                               |
    | ------------------------------------------------- | -------------------------------------------------- |
    | "4"^^<http://www.w3.org/2001/XMLSchema#integer>   | "1894"^^<http://www.w3.org/2001/XMLSchema#integer> |
    | "57"^^<http://www.w3.org/2001/XMLSchema#integer>  | "1895"^^<http://www.w3.org/2001/XMLSchema#integer> |
    | "106"^^<http://www.w3.org/2001/XMLSchema#integer> | "1896"^^<http://www.w3.org/2001/XMLSchema#integer> |
    | "99"^^<http://www.w3.org/2001/XMLSchema#integer>  | "1897"^^<http://www.w3.org/2001/XMLSchema#integer> |
    | "122"^^<http://www.w3.org/2001/XMLSchema#integer> | "1898"^^<http://www.w3.org/2001/XMLSchema#integer> |
    | "473"^^<http://www.w3.org/2001/XMLSchema#integer> | "1899"^^<http://www.w3.org/2001/XMLSchema#integer> |
    | "263"^^<http://www.w3.org/2001/XMLSchema#integer> | "1900"^^<http://www.w3.org/2001/XMLSchema#integer> |
    | "158"^^<http://www.w3.org/2001/XMLSchema#integer> | "1901"^^<http://www.w3.org/2001/XMLSchema#integer> |
    | "133"^^<http://www.w3.org/2001/XMLSchema#integer> | "1902"^^<http://www.w3.org/2001/XMLSchema#integer> |
    | "85"^^<http://www.w3.org/2001/XMLSchema#integer>  | "1903"^^<http://www.w3.org/2001/XMLSchema#integer> |

#### 6b. How many film programmes are stored in the database per year?

This query counts the number of film programmes, independent of the number of films screened. The filter excludes programmes without films (with only live theatrical performances). When filtering programmes without ScreeningEvents, the earliest year is 1893, which makes sense because it predates the invention of film.

??? Example  "Amount of film programmes stored in the database per year"

    ```sparql
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX schema: <http://schema.org/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    
    SELECT (COUNT(?programme) AS ?nr_of_pr) ?year WHERE {
      GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {
        
         ?programme a schema:Event;
        	          schema:startDate ?date .
    
     FILTER EXISTS {
        ?programme schema:subEvent [a schema:ScreeningEvent] .
    }
          }
    } GROUP BY (YEAR(?date) AS ?year) ORDER BY (?year)
    ```

    Results (first 10 rows)

    | nr_of_pr           | year                |
    | ------------------ | ------------------- |
    | "1"^^xsd:integer   | "1894"^^xsd:integer |
    | "17"^^xsd:integer  | "1895"^^xsd:integer |
    | "25"^^xsd:integer  | "1896"^^xsd:integer |
    | "37"^^xsd:integer  | "1897"^^xsd:integer |
    | "51"^^xsd:integer  | "1898"^^xsd:integer |
    | "131"^^xsd:integer | "1899"^^xsd:integer |
    | "96"^^xsd:integer  | "1900"^^xsd:integer |
    | "51"^^xsd:integer  | "1901"^^xsd:integer |
    | "33"^^xsd:integer  | "1902"^^xsd:integer |
    | "25"^^xsd:integer  | "1903"^^xsd:integer |

#### 7. For which cities can we find film screenings in the database? How many screenings? In which year? 

??? Example  "Amount of screenings per city in the database"

    ```sparql
    PREFIX schema: <http://schema.org/>
    
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    SELECT ?city (COUNT(?programme) AS ?nrofprogrammes) {
    GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {
    
    ?programme a schema:Event ;
                schema:location ?venue .
      
    ?venue a schema:MovieTheater ;
              schema:location ?place .
      
    ?place a schema:Place ;
                schema:address [ schema:addressLocality ?city ] .
    
      }
    } GROUP BY ?city ORDER BY DESC(?nrofprogrammes)
    
    ```

#### 8. How many active cinemas are stored in the database, per year? Can we plot this on a map?
#### 9. Where were traveling cinemas active, per year? Can we plot this on a map?


