# Model

These samples from the RDFdata show most of the properties and classes used. This overview including the examples was retrieved by querying:

??? Abstract "Retrieve classes and their properties"
    === "SPARQL"
	```SPARQL
	PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
	PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
	PREFIX dcterms: <http://purl.org/dc/terms/>
	PREFIX schema: <http://schema.org/>
	PREFIX sem: <http://semanticweb.cs.vu.nl/2009/11/sem/>
	PREFIX gleio: <http://lei.info/gleio/>
	PREFIX geo: <http://www.opengis.net/ont/geosparql#>
	PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
	PREFIX pext: <http://www.ontotext.com/proton/protonext#> 
	PREFIX pnv: <https://w3id.org/pnv#>
	PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

	SELECT ?type (?pred AS ?Property) (?desc AS ?Description) (SAMPLE(?obj) AS ?Example) WHERE {
		GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {
			[] a ?type;
			   ?pred ?obj.
		}
		OPTIONAL { ?pred rdfs:comment ?desc } # if the ontology is loaded
		
	} GROUP BY ?type ?pred ?desc ORDER BY ?type ?pred
	```

## Overview of classes

!!! Warning "The [`schema:Role`](http://schema.org/Role) class"
	Some properties on the resources are temporally restricted. To express a specific time frame in which a certain property value relation is valid, the [`schema:Role`](http://schema.org/Role) class is used. This class can be used in any object position and extends the triple with the same property, while incorporating additional information, such as specific roles some entity played, or a start and end date. The Role class is used consistently in the data, even when there is nothing to reify. 

!!! Notice "Simple Event Model Timestamps (`sem:hasTimeStamp`)"
	If a date is given in a less precise format than a full ISO 8601 description (i.e. YYYY-mm-dd, given as `xsd:date`), then the [Simple Event Model Timestamp](https://semanticweb.cs.vu.nl/2009/11/sem/#sem:hasTimeStamp) properties have been used to supply a proper `xsd:date` value. 
	
	For example, if a programme is known to be shown in `schema:startDate "1932-05"^^xsd:gYearMonth`, then the resource also includes the properties `sem:hasEarliestBeginTimeStamp "1932-05-01"^^xsd:date` and `sem:hasLatestEndTimeStamp "1932-05-31"^^xsd:date`. For consistency and queryablity, the `sem` properties are always present, even if an exact date is given.

	These properties are also included in the `schema:Role` extensions of some predicates to indicate a temporal restriction or (un)certainty.

### Films
Each movie in the Cinema Context database is an instance of the [`schema:Movie`](http://schema.org/Movie) class. Its permalink is used as URI. The following properties can be found as properties of a Movie resource:

| Property                      | Description                                                                                                                                                                                                                                                           | Example                                                |
| ----------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------ |
| rdf:type                      | A resource of this type is an instance of this class.                                                                                                                                                                                                                 | schema:Movie                                           |
| dcterms:extent                | The length of the movie's film roll. Modelled as a `schema:PropertyValue`.                                                                                                                                                                                            | _:blankNode                                            |
| dcterms:format                | The width of the film gauge                                                                                                                                                                                                                                           | "16mm"                                                 |
| schema:alternateName          | Alternative names for the film. This can be an alternative title used in a particular screening. This name is also included in the [schema:ScreeningEvent](#events) if there are screenings known for a film. The language of the title is given in the language tag. | "Hoffmanns vertellingen"@nl                            |
| schema:contentRating          | A rating (a `schema:Rating` and `schema:CreativeWork`) of the film.                                                                                                                                                                                                   | <<http://www.cinemacontext.nl/id/rating/Dossier10008>> |
| schema:countryOfOrigin        | The country of the film's producer modelled as a blank node of type `schema:Country`. When available, a `owl:sameAs` property is included to refer to the [TGN](http://www.getty.edu/research/tools/vocabularies/tgn/) and Wikidata.                                  | _:blankNode                                            |
| schema:director               | Director of the film                                                                                                                                                                                                                                                  | "Georges Monca"                                        |
| schema:dateCreated            | The date or year the movie was produced                                                                                                                                                                                                                               | "1932"^^xsd:gYear                                      |
| sem:hasEarliestBeginTimeStamp | The earliest possible date the film was made or published, given as `xsd:date`.                                                                                                                                                                                       | "1932-01-01"^^xsd:date                                 |
| sem:hasLatestEndTimeStamp     | The latest possible date the film was made or published, given as `xsd:date`.                                                                                                                                                                                         | "1932-12-31"^^xsd:date                                 |
| schema:description            | Any additional information or comments.                                                                                                                                                                                                                               | "twee delen"                                           |
| schema:genre                  | The genre of the film                                                                                                                                                                                                                                                 | "fiction"                                              |
| schema:isPartOf               | The film or episode can be part of a series.                                                                                                                                                                                                                          | <http://www.cinemacontext.nl/id/F030942>               |
| schema:name                   | The title of the film                                                                                                                                                                                                                                                 | "Faithless (1932)"                                     |
| schema:publisher              | Used to include the distributor of a film (a `schema:Organization`). Modelled through the `schema:Role` class.                                                                                                                                                        | _:blankNode                                            |
| schema:sameAs                 | A film's respective IMDb entry.                                                                                                                                                                                                                                       | <<https://www.imdb.com/title/tt0024780>>               |

!!! Tip "IMDb"
	If known, a `schema:sameAs` refers to the respective movie in the Internet Movie Database.

**Example**

=== "Turtle (.ttl)"
```turtle
<http://www.cinemacontext.nl/id/F002877>
	schema:name "Daydreams (1922)" ;
	schema:description "Buster Keaton" ;
	schema:dateCreated "1922"^^xsd:gYear ;
	schema:sameAs <https://www.imdb.com/title/tt0013055> ;
	schema:countryOfOrigin _:USA ;
	dcterms:extent [
		schema:value "800"^^xsd:int ;
		schema:unitCode "MTR" ;
		a schema:PropertyValue ;
	] ;
	dcterms:format "35mm" ;
	schema:genre "fiction" ;
	schema:publisher [
		schema:publisher <http://www.cinemacontext.nl/id/R000097> ;
		a schema:Role ; 
	] ;
	schema:alternateName "Van binnen moet je me zien"@nl ;
	a schema:Movie .

_:USA a schema:Country ;
	schema:name "Verenigde Staten van Amerika"@nl ;
	schema:name "United States"@en ;
	owl:sameAs <http://www.wikidata.org/entity/Q30> ;
	owl:sameAs <http://vocab.getty.edu/tgn/7012149> .
```

!!! Tip "Querying on Country"
	The country of a film's origin is stored in the `schema:countryOfOrigin` property, which is modelled as a blank node of type `schema:Country`. Both a Dutch and English `schema:name` label are included, as well as (if available) `owl:sameAs` links to Wikidata or the [TGN](http://www.getty.edu/research/tools/vocabularies/tgn/). To query for films from the Soviet Union:

	=== "SPARQL"
	```SPARQL
	PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
	PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
	PREFIX schema: <http://schema.org/>

	SELECT ?film WHERE {
	GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {

		?film a schema:Movie ;
			schema:countryOfOrigin [ a schema:Country ;
									schema:name "Soviet Union"@en ] . # mind the language tag!
		}
	}
	```

### Theaters

Theaters, or 'venues' are organizations that are located on a specific location and have a specific name. Usually they are owned by a [company](#companies). Each theater is an instance of the [`schema:MovieTheater`](http://schema.org/MovieTheater) class. The following properties can be found as properties of a MovieTheater resource:

| Property                       | Description                                                                                                                                                      | Example                                                    |
| ------------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------- |
| rdf:type                       | A resource of this type is an instance of this class.                                                                                                            | schema:MovieTheater                                        |
| schema:name                    | The name of the theater                                                                                                                                          | "Actania"                                                  |
| schema:description             | Any additional information or comments.                                                                                                                          | "Tussen 1965-68 gesloten resp. niet ingeschreven bij NBB." |
| schema:citation                | Refers to a publication (article, book etc.) about the film.    Modelled through the `schema:Role` class.                                                        | _:blankNode                                                |
| schema:containedInPlace        | This cinema is operated/housed in a building (externally defined on Wikidata or BAG). See the info box below for more info.                                      | <<https://www.wikidata.org/wiki/Q1843049>>                 |
| schema:parentOrganization      | When a MovieTheater is owned by a company, the company (a `schema:Organization`) is given as a value of this property. Modelled through the `schema:Role` class. | _:blankNode                                                |
| schema:location                | The location of the theater. Refers to a `schema:Place`.                                                                                                         | <<http://www.cinemacontext.nl/id/place/Perc1025>>          |
| schema:temporalCoverage        | The earliest begin and latest end date of the lifespan of a theater. Expressed with `sem` timestamp properties.                                                  | _:blankNode                                                |
| schema:maximumAttendeeCapacity | Maximum seating capacity of the theater. Modelled through the `schema:Role` class.                                                                               | _:blankNode                                                |
| schema:screenCount             | Number of available screens in the theater. Modelled through the `schema:Role` class.                                                                            | _:blankNode                                                |
| schema:additionalType          | Used in case of a 'mobile theater'. Refers to WikiData [`wd:Q6605486`](https://www.wikidata.org/wiki/Q6605486) (Traveling cinema)                                | <<http://www.wikidata.org/entity/Q6605486>>                |


!!! Tip "schema:containedInPlace"
	The `schema:containedInPlace` property is used on Theaters to express a relation to a resource from Wikidata or the BAG, due to a somewhat unclear and amgibuous distinction between organizations and buildings. This property states that a cinema is being operated in a building. For Wikidata, most of these resources are defined as subclass of `Venue`. The Cinema Context ID [`P8296`](http://www.wikidata.org/prop/direct/P8296) property can be the inverse of this.

	This data is extracted from Wikidata and a separate file  with these links is included in the data. For an up-to-date overview, try this construct query: 

	=== "SPARQL"
	```SPARQL
	PREFIX wdt: <http://www.wikidata.org/prop/direct/>
	PREFIX wd: <http://www.wikidata.org/entity/>
	PREFIX schema: <http://schema.org/>

	CONSTRUCT {
		?ccURI schema:containedInPlace ?wd .
	} WHERE {
		
		SERVICE <https://query.wikidata.org/sparql> {
			?wd wdt:P8296 ?ccid .
			BIND(URI(CONCAT("http://www.cinemacontext.nl/id/", ?ccid)) AS ?ccURI)
		}

		{
			SELECT * WHERE {
			GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {
				?ccURI a ?venueType .        
				FILTER(?venueType in (schema:MovieTheater, schema:EventVenue))
				}
			}
		}
	}
	```

**Example**

=== "Turtle (.ttl)"
```turtle
<http://www.cinemacontext.nl/id/B000006>
	schema:name "Cineac Damrak" ;
	schema:location <http://www.cinemacontext.nl/id/place/Perc8> ;
	schema:parentOrganization [
		schema:parentOrganization <http://www.cinemacontext.nl/id/R000007> ;
		sem:hasEarliestBeginTimeStamp "1938-01-01"^^xsd:date ;
		sem:hasLatestBeginTimeStamp "1938-12-31"^^xsd:date ;
		sem:hasEarliestEndTimeStamp "1983-01-01"^^xsd:date ;
		sem:hasLatestEndTimeStamp "1983-12-31"^^xsd:date ;
		a schema:Role ; 
	] ;
	schema:temporalCoverage [
		sem:hasEarliestBeginTimeStamp "1938-03-17"^^xsd:date ;
		sem:hasLatestBeginTimeStamp "1938-03-17"^^xsd:date ;
		sem:hasEarliestEndTimeStamp "1983-07-06"^^xsd:date ;
		sem:hasLatestEndTimeStamp "1983-07-06"^^xsd:date ;
	] ;
	schema:screenCount [
		schema:screenCount "2"^^xsd:int ;
		sem:hasEarliestBeginTimeStamp "1975-01-01"^^xsd:date ;
		sem:hasLatestBeginTimeStamp "1975-01-31"^^xsd:date ;
		a schema:Role ;
	] ;
	schema:screenCount [
		schema:screenCount "1"^^xsd:int ;
		sem:hasEarliestBeginTimeStamp "1938-01-01"^^xsd:date ;
		sem:hasLatestBeginTimeStamp "1938-12-31"^^xsd:date ;
		a schema:Role ;
	] ;
	schema:maximumAttendeeCapacity [
		schema:maximumAttendeeCapacity "750"^^xsd:int ;
		sem:hasLatestBeginTimeStamp "1938-12-31"^^xsd:date ;
		a schema:Role ; 
	] ;
	schema:citation [
		schema:citation <http://www.cinemacontext.nl/id/publication/Pub165> ;
		a schema:Role ; 
	] ;
	a schema:MovieTheater .
```

#### Mobile theaters

Mobile theaters are like MovieTheaters, in the sense that you could go there to see a movie. And they are located at a schema:Place, at least at a specific moment in time. Mobile cinemas have an additionalType of [`wd:Q6605486`](http://www.wikidata.org/entity/Q6605486) (mobile cinema) as well.

**Example**

=== "Turtle (.ttl)"
```turtle
<http://www.cinemacontext.nl/id/stand/001973>
	schema:name "Mobile Theatre" ;
	schema:location <http://www.cinemacontext.nl/id/place/id1140887559002> ;
	schema:additionalType wd:Q6605486 ;
	a schema:MovieTheater .
``` 
#### Event venues

A `schema:EventVenue` might be a hotel, a concerthall or even a church.

**Example**

=== "Turtle (.ttl)"
```turtle
<http://www.cinemacontext.nl/id/eventvenue/001744>
	schema:name "Hollandsche Schouwburg" ;
	schema:location <http://www.cinemacontext.nl/id/place/id1104540487036> ;
	a schema:EventVenue .
```


### Companies

A company is the business or organization that runs one or multiple theatres. They are modelled as an instance of [`schema:Organization`](http://schema.org/Organization) and can have the following properties:

| Property                      | Description                                                                                                                                                                                                    | Example                                                                                                           |
| ----------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------- |
| rdf:type                      | A resource of this type is an instance of this class.                                                                                                                                                          | schema:Organization                                                                                               |
| dcterms:source                | Archival sources in which the company is described.                                                                                                                                                            | <http://www.cinemacontext.nl/id/archivalsource/id1131235207810>                                                   |
| gleio:hasLegalForm            | Used to indicate the type of company (e.g. B.V.). This is modelled as a blank node of type `gleio:LegalForm` with a standardized legal form code. The name of the legal form is included in a `skos:altLabel`. | _:blankNode                                                                                                       |
| pext:activeInSector           | The industry sector in which the company is active (e.g. "bioscoopexploitatie"). Modelled as a blank node of type `pext:IndustrySector`. The name of the sector is included in a `schema:name`.                | _:blankNode                                                                                                       |
| schema:alternateName          | Alternative name for the company.                                                                                                                                                                              | "Sprekende Bioscope"                                                                                              |
| schema:citation               | Modelled through the `schema:Role` class.                                                                                                                                                                      | _:blankNode                                                                                                       |
| schema:description            | Any additional information or comments.                                                                                                                                                                        | "Zie Scheepmaker en zie ook Abrassart. Beiden gebruiken in dezelfde tijd de benaming Cinematograph Pathé Frères." |
| schema:name                   | The name of the company.                                                                                                                                                                                       | "Pathé Frères Reizende Bioscoop (Amsterdam)"                                                                      |
| schema:parentOrganization     | Modelled through the `schema:Role` class.                                                                                                                                                                      | _:blankNode                                                                                                       |
| sem:hasEarliestBeginTimeStamp | Earliest possible begin date for the time of operation of this company (its existence), given as `xsd:date`.                                                                                                   | "1907-01-01"^^xsd:date                                                                                            |
| sem:hasLatestBeginTimeStamp   | latest possible begin date for the time of operation of this company (its existence), given as `xsd:date`.                                                                                                     | "1907-12-31"^^xsd:date                                                                                            |
| sem:hasEarliestEndTimeStamp   | Earliest possible end date for the time of operation of this company (its existence), given as `xsd:date`.                                                                                                     | "1916-01-01"^^xsd:date                                                                                            |
| sem:hasLatestEndTimeStamp     | Latest possible end date for the time of operation of this company (its existence), given as `xsd:date`.                                                                                                       | "1916-12-31"^^xsd:date                                                                                            |


	





!!! Tip "Entity Legal Form (ELF)"
	We make use of the [GLEIO ontology](http://lei.info/gleio/) to express the legal structure of the company. The `gleio:hasEntityLegalFormCode` property refers to codes from the ISO Entity Legal Form (ELF, [ISO 20275](https://www.gleif.org/en/about-lei/code-lists/iso-20275-entity-legal-forms-code-list)) code list maintained by [GLEIF](https://www.gleif.org/). The Dutch legal form name is included in a `skos:altLabel`. 

     | Property                      | Description                                                                                                                                                                                                           | Example              |
     | ----------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------- |
     | rdf:type                      | A resource of this type is an instance of this class.                                                                                                                                                                 | gleio:LegalForm      |
     | gleio:hasEntityLegalFormCode  | The reference code of the legal form of the entity, taken from the [ISO Entity Legal Form (ELF) code list](https://www.gleif.org/en/about-lei/code-lists/iso-20275-entity-legal-forms-code-list) maintained by GLEIF. | "V44D"               |
     | sem:hasEarliestBeginTimeStamp | Earliest possible begin date for which this Legal Form is valid for this company, given as `xsd:date`.                                                                                                                | 1973-01-01^^xsd:date |
     | sem:hasEarliestEndTimeStamp   | Latest possible begin date for which this Legal Form is valid for this company, given as `xsd:date`.                                                                                                                  | 1929-01-01^^xsd:date |
     | sem:hasLatestBeginTimeStamp   | Earliest possible end date for which this Legal Form is valid for this company, given as `xsd:date`.                                                                                                                  | 1938-12-31^^xsd:date |
     | sem:hasLatestEndTimeStamp     | Latest possible end date for which this Legal Form is valid for this company, given as `xsd:date`.                                                                                                                    | 1940-12-31^^xsd:date |
     | skos:altLabel                 | A readable Dutch label of the legal form. In the Cinema Context database, possible options are: `B.V.`, `C.V.`, `Firma`, `N.V.`, `Stichting`, and `Vereniging`.                                                       | "Stichting"          |

**Example**

=== "Turtle (.ttl)"
```turtle
@prefix gleio: <http://lei.info/gleio/> .
@prefix pext: <http://www.ontotext.com/proton/protonext#> .
@prefix schema: <http://schema.org/> .
@prefix sem: <http://semanticweb.cs.vu.nl/2009/11/sem/> . 

<http://www.cinemacontext.nl/id/R000017>
	schema:name "Eindhovensche Bioscope Maatschappij NV" ;
	sem:hasEarliestBeginTimeStamp "1913-07-01"^^xsd:date ;
	sem:hasLatestBeginTimeStamp "1913-07-31"^^xsd:date ;
	schema:parentOrganization [
		schema:parentOrganization <http://www.cinemacontext.nl/id/R000180> ;
		schema:description "Eindhovense Bioscope Mij. neemt in 1939 de aandelen van D. Hamburger over." ;
		sem:hasEarliestBeginTimeStamp "1929-01-01"^^xsd:date ;
		sem:hasLatestBeginTimeStamp "1929-12-31"^^xsd:date ;
		a schema:Role ;
	] ;
	pext:activeInSector [
		schema:name "bioscoopexploitatie" ;
		a pext:IndustrySector ;
	] ;
	gleio:hasLegalForm [
		a gleio:LegalForm ;
		skos:altLabel "N.V." ;
		gleio:hasEntityLegalFormCode "B5PM" ;
	] ;
	schema:citation [
		schema:citation <http://www.cinemacontext.nl/id/publication/id1151255550914> ;
		a schema:Role ;
	] ;
	a schema:Organization .
```

### Persons

A person might work for a company. Each person is modelled as an instance of [`schema:Person`](http://schema.org/Person) and can have the following properties:

| Property         | Description                                                                                                                                                                                        | Example                |
| ---------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------- |
| rdf:type         | A resource of this type is an instance of this class.                                                                                                                                              | schema:Person          |
| schema:name      | The name of a person as one string literal.                                                                                                                                                        | "Dirk Rosenga"         |
| pnv:hasName      | The name of a person, split out in the [pnv](https://w3id.org/pnv#) vocabulary. Points to a `pnv:PersonName`.                                                                                      | _:personName           |
| schema:worksFor  | The company the person works for. This points to a `schema:OrganizationRole` that holds extra information on the role of the person in the company, and the time at which the person worked there. | _:organizationRole     |
| schema:birthDate | The birth date of a person.                                                                                                                                                                        | "1874-12-30"^^xsd:date |
| schema:deathDate | The death date of a person.                                                                                                                                                                        | "1943-03-13"^^xsd:date |
| schema:gender    | The gender of a person (e.g. `schema:Female` or `schema:Male`).                                                                                                                                    | schema:Female          |


!!! Tip "Person Name Vocabulary (PNV)"
	Every person has a name. Beside using the generic `schema:name` property, we also model a `pnv:PersonName` entity as the value of the `pnv:hasName` property. This is according to the [Person Name Vocabulary](http://w3id.org/pnv#) (PNV), an rdf vocabulary and data model for persons' names.

    | Property          | Description                                                                                         | Example          |
    | ----------------- | --------------------------------------------------------------------------------------------------- | ---------------- |
    | rdf:type          | A resource of this type is an instance of this class.                                               | pnv:PersonName   |
    | pnv:baseSurname   | A person's surname without a prefix, such as 'van' or 'de').                                        | "Wersch-Delhaye" |
    | pnv:givenName     | A person's first name, how this person is called.                                                   | "Jan"            |
    | pnv:initials      | Instead of a givenName, the person's initials.                                                      | "H.A."           |
    | pnv:literalName   | The full name of the person. Includes all `pnv` components.                                         | "Corry Tendeloo" |
    | pnv:surname       | A person's surname, which is a combination of the `pnv:surnamePrefix` and `pnv:baseSurname` values. | "Walpot"         |
    | pnv:surnamePrefix | The prefix of the `pnv:baseSurname`, such as 'de', 'van', or 'l''.                                  | "v.d."           |

**Example**

=== "Turtle (.ttl)"
```turtle
<http://www.cinemacontext.nl/id/P000015>
	schema:name "mevr. W. Gunneman" ;
	schema:gender schema:Female ;
	pnv:hasName [
		pnv:literalName "W. Gunneman" ;
		pnv:initials "W." ;
		pnv:surname "Gunneman" ;
		a pnv:PersonName ;
	] ;
	schema:worksFor [
		a schema:OrganizationRole ;
		schema:roleName "bedrijfsleider" ;
		schema:worksFor <http://www.cinemacontext.nl/id/B000006> ;
		sem:hasEarliestBeginTimeStamp "1977-01-01"^^xsd:date ;
		sem:hasLatestBeginTimeStamp "1977-12-31"^^xsd:date ;
	] ;
	a schema:Person .
```



### Places

A 'place' is just that: a point on the map. Sometimes a place might have a 1:1 relation with a building, but it's perfectly possible there were several buildings on that spot, over time.

| Property           | Description                                                                                                                                          | Example                                                                                                   |
| ------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------- |
| rdf:type           | A resource of this type is an instance of this class.                                                                                                | schema:Place                                                                                              |
| schema:address     | A place's physical address. Points to a blank node of type `schema:PostalAddress`.                                                                   | _:postaladdress                                                                                           |
| schema:citation    | A place can be mentioned in a CreativeWork, Book, or Article. Modelled through the schema:Role class. Points to a `schema:CreativeWork` or subtypes. | _:citation                                                                                                |
| schema:description | Description of the place.                                                                                                                            | "Hotel Van Stralen, 's Lands Welvaren. Toneelzaal op bovenverdieping. Gemeente Boornsterhem (Friesland)." |
| geo:hasGeometry    | The geometry (e.g. point) of a place. Expressed as a blank node with the geometry as WKT in geo:wktLiteral.                                          | _:geometry                                                                                                |

**Example**

=== "Turtle (.ttl)"
```turtle
<http://www.cinemacontext.nl/id/place/Perc12>
	geo:hasGeometry [
		geo:asWKT "POINT(4.88363 52.36333)" ;
	] ;
	schema:address [
		schema:streetAddress "Kleine-Gartmanplantsoen 13 - 17" ;
		schema:addressLocality "Amsterdam" ;
		a schema:PostalAddress ;
	] ;
	schema:citation [
		schema:citation <http://www.cinemacontext.nl/id/publication/Pub166> ;
		schema:description "City, Amsterdam, van architect Jan Wils. Themanummer." ;
		a schema:Role ; 
	] ;
	a schema:Place .
```

### Events

We distinguish two event classes: `schema:Event` (prototypical events for which one can buy tickets) and `sem:Event` (generic events, things that happen).

#### Programmes

A 'programme' is a [`schema:Event`](http://schema.org/Event) consisting of one or more instances of type [`schema:ScreeningEvent`](http://schema.org/ScreeningEvent) (filmic) or `schema:TheaterEvent`(http://schema.org/TheaterEvent) (non-filmic), that was held in a specific theater on one or more dates. Sometimes a film was screened under another name (a Dutch name, for example). In such cases this name is mentioned as a `schema:alternateName`. The `schema:startDate` indicates the day a programme started. Normally, a cinema programme would be screened for a week. Deviations from this norm are usually annotated as `schema:description`. 

| Property                      | Description                                                                                                                                              | Example                                    |
| ----------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------ |
| rdf:type                      | A resource of this type is an instance of this class.                                                                                                    | schema:Event                               |
| schema:citation               | A programme can be mentioned in a CreativeWork, Book, or Article. Modelled through the schema:Role class. Points to a `schema:CreativeWork` or subtypes. | _:citation                                 |
| schema:description            | Additional info for the programme.                                                                                                                       | "6-14 oktober 1907"                        |
| schema:location               | A programme has a location. Points to a `schema:Place`.                                                                                                  | <<http://www.cinemacontext.nl/id/B000132>> |
| schema:name                   | Title of the programme.                                                                                                                                  | "Jeugdbioscoop"                            |
| schema:organizer              | A programme can be organized by a company. Points to a `schema:Organization`.                                                                            | <<http://www.cinemacontext.nl/id/R001364>> |
| schema:startDate              | The date of the programme.                                                                                                                               | "1942-12"^^xsd:gYearMonth                  |
| sem:hasTimeStamp              | The date of the programme given as `xsd:date`.                                                                                                           | "1942-12-06"^^xsd:date                     |
| sem:hasEarliestBeginTimeStamp | The earliest possible begin date of the programme given as `xsd:date`.                                                                                   | “1942-12-01”^^xsd:date                     |
| sem:hasLatestBeginTimeStamp   | The latest possible begin date of the programme given as `xsd:date`.                                                                                     | “1942-12-31”^^xsd:date                     |
| sem:hasEarliestEndTimeStamp   | The earliest possible end date of the programme given as `xsd:date`.                                                                                     | “1942-12-01”^^xsd:date                     |
| sem:hasLatestEndTimeStamp     | The latest possible end date of the programme given as `xsd:date`.                                                                                       | “1942-12-31”^^xsd:date                     |
| schema:subEvent               | A program can be made up of one or more subEvents (which can be ordered).                                                                                | _:screeningEvent or _:theaterEvent         |


##### ScreeningEvent

| Property             | Description                                                                      | Example                                               |
| -------------------- | -------------------------------------------------------------------------------- | ----------------------------------------------------- |
| rdf:type             | A resource of this type is an instance of this class.                            | schema:ScreeningEvent                                 |
| schema:alternateName | The name that was used for the film in the programme. May contain a languagetag. | "Moskou Shanghai"@nl                                  |
| schema:position      | Position of the subEvent in the programme. If known.                             | "2"^^xsd:int                                          |
| schema:workPresented | The film that was presented.                                                     | <<http://www.cinemacontext.nl/id/episode/Serie100.2>> |

##### TheaterEvent

| Property             | Description                                                                                                                                      | Example             |
| -------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------- |
| rdf:type             | A resource of this type is an instance of this class.                                                                                            | schema:TheaterEvent |
| schema:position      | Position of the subEvent in the programme. If known.                                                                                             | "2"^^xsd:int        |
| schema:workPerformed | The show that was performed. Points to a blank node of type `schema:CreativeWork` that holds the name of the show in the `schema:name` property. | _:creativeWork      |

**Example**

A programme has multiple ordered subEvents of type `schema:ScreeningEvent`. 

=== "Turtle (.ttl)"
```turtle
<http://www.cinemacontext.nl/id/V062775>
	schema:startDate "1918-10-25"^^xsd:date ;
	schema:location <http://www.cinemacontext.nl/id/B001452> ;
	schema:subEvent [
		schema:workPresented <http://www.cinemacontext.nl/id/F031808> ;
		schema:position "1"^^xsd:int ;
		schema:alternateName "Regimentsdochter, De" ;
		schema:inLanguage "NL" ;
		a schema:ScreeningEvent ;
	] ;
	schema:subEvent [
		schema:workPresented <http://www.cinemacontext.nl/id/F007737> ;
		schema:position "2"^^xsd:int ;
		schema:alternateName "Prinses van Neutralië, De" ;
		schema:inLanguage "NL" ;
		a schema:ScreeningEvent ;
	] ;
	schema:subEvent [
		schema:workPresented <http://www.cinemacontext.nl/id/F011327> ;
		schema:position "3"^^xsd:int ;
		schema:alternateName "Medaillon, Het" ;
		schema:inLanguage "NL" ;
		a schema:ScreeningEvent ;
	] ;
	schema:citation [
		schema:citation <http://www.cinemacontext.nl/id/publication/id1113821541222> ;
		schema:description "Krant uit de week van de voorstelling" ;
		a schema:Role ;
	] ;
	a schema:Event .
```

Or has both subEvents of type `schema:ScreeningEvent` and `schema:TheaterEvent`. 

=== "Turtle (.ttl)"
```turtle
<http://www.cinemacontext.nl/id/V107294>
	sem:hasTimeStamp "1923-08-31"^^xsd:date ;
	sem:hasEarliestBeginTimeStamp "1923-08-31"^^xsd:date ;
	sem:hasLatestEndTimeStamp "1923-08-31"^^xsd:date ;
	schema:startDate "1923-08-31"^^xsd:date ;
	schema:location <http://www.cinemacontext.nl/id/B000027> ;
	schema:subEvent [
		schema:workPerformed [ a schema:CreativeWork ;
			 schema:name "Mello Wendini, \"komisch dressuur act\"" ] ;
		a schema:TheaterEvent ;
	] ;
	schema:subEvent [
		schema:workPresented <http://www.cinemacontext.nl/id/F005786> ;
		a schema:ScreeningEvent ;
	] ;
	schema:citation [
		schema:citation <http://www.cinemacontext.nl/id/publication/id1117374433554> ;
		schema:description "Krant uit de week van de voorstelling" ;
		a schema:Role ;
	] ;
	a schema:Event .
```

**Example (mobile theater)**

If the location where the `schema:Event` was held is a mobile theater, the event is likely to have a `schema:organizer` property that presumably points to the mobile theater company.


=== "Turtle (.ttl)"
```turtle
<http://www.cinemacontext.nl/id/V087704>
	sem:hasEarliestBeginTimeStamp "1903-05-01"^^xsd:date ;
	sem:hasLatestEndTimeStamp "1903-05-31"^^xsd:date ;
	schema:startDate "1903-05"^^xsd:gYearMonth ;
	schema:name "Mobile Theatre" ;
	schema:description "Sint Servaaskermis" ;
	schema:location <http://www.cinemacontext.nl/id/stand/002111> ;
	schema:organizer <http://www.cinemacontext.nl/id/R001354> ;
	schema:subEvent [
		schema:workPresented <http://www.cinemacontext.nl/id/F032389> ;
		a schema:ScreeningEvent ;
	] ;
	schema:subEvent [
		schema:workPresented <http://www.cinemacontext.nl/id/F032507> ;
		a schema:ScreeningEvent ;
	] ;
	schema:subEvent [
		schema:workPresented <http://www.cinemacontext.nl/id/F032461> ;
		a schema:ScreeningEvent ;
	] ;
	schema:subEvent [
		schema:workPresented <http://www.cinemacontext.nl/id/F032464> ;
		a schema:ScreeningEvent ;
	] ;
	schema:subEvent [
		schema:workPresented <http://www.cinemacontext.nl/id/F032428> ;
		schema:alternateName "Paus Leo XIII in zijn laatste levensdagen"@nl ;
		a schema:ScreeningEvent ;
	] ;
	schema:subEvent [
		schema:workPresented <http://www.cinemacontext.nl/id/F032368> ;
		schema:alternateName "Soesman en Zwaaf zingen Bokkie-Bè"@nl ;
		a schema:ScreeningEvent ;
	] ;
	schema:citation [
		schema:citation <http://www.cinemacontext.nl/id/publication/Pub503> ;
		a schema:Role ;
	] ;
	a schema:Event .
```



#### Construction history

Construction events include the sem:EventType 'Nieuwbouw' and 'Verbouwing'. Construction events are of the class `sem:Event`, which helps to distinguish these events from 'programmes' - modelled as `schema:Event`.

| Property                      | Description                                                                                                                                                                                                                | Example                                                      |
| ----------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------ |
| rdf:type                      | A resource of this type is an instance of this class.                                                                                                                                                                      | sem:Event                                                    |
| schema:description            | Additional info of the building and the building construction.                                                                                                                                                             | "ontwerp van J.J.P. Oud, uitgevoerd door zijn zoon Hans Oud" |
| sem:eventType                 | Type of construction. Refers to a blank node of type `sem:EvenType`. The name of this type is given in a `schema:name` property and can be `"Nieuwbouw"@nl`, `"Construction"@en`, `"Verbouwing"@nl`, `"Refurbishment"@en`. | _:eventtype                                                  |
| sem:hasActor                  | Any actors (e.g. persons) that are involved in this event. Modelled as `sem:Role` with the type of role given as `sem:roleType`. See below for an example of how this has been modelled.                                   | _:actor                                                      |
| sem:hasEarliestBeginTimeStamp | Earliest possible begin date of the construction, given as `xsd:date`.                                                                                                                                                     | "1928-01-01"^^xsd:date                                       |
| sem:hasEarliestEndTimeStamp   | Latest possible begin date of the construction, given as `xsd:date`.                                                                                                                                                       | "2008-01-01"^^xsd:date                                       |
| sem:hasLatestBeginTimeStamp   | Earliest possible end date of the construction, given as `xsd:date`.                                                                                                                                                       | "1913-12-31"^^xsd:date                                       |
| sem:hasLatestEndTimeStamp     | Latest possible end date of the construction, given as `xsd:date`.                                                                                                                                                         | "1957-12-31"^^xsd:date                                       |
| sem:hasPlace                  | The location of the event. Points to an instance of type `schema:Place`. See also: [Places](#places)                                                                                                                       | <<http://www.cinemacontext.nl/id/place/Perc75>>              |


**Example**

=== "Turtle (.ttl)"
```turtle
<http://www.cinemacontext.nl/id/constructionevent/Perc188-01>
	sem:hasPlace <http://www.cinemacontext.nl/id/place/Perc188> ;
	sem:eventType _:nieuwbouw ;
	sem:hasEarliestBeginTimeStamp "1914-01-01"^^xsd:date ;
	sem:hasLatestBeginTimeStamp "1914-12-31"^^xsd:date ;
	sem:hasEarliestEndTimeStamp "1914-01-01"^^xsd:date ;
	sem:hasLatestEndTimeStamp "1914-12-31"^^xsd:date ;
	sem:hasActor [
		rdf:value <http://www.cinemacontext.nl/id/P002395> ;
		sem:roleType [
			schema:name "architect" ; 
			a sem:RoleType ;
		] ;
		a sem:Role ;
	] ;
	a sem:Event .

_:nieuwbouw a sem:EventType ; 
	schema:name "Nieuwbouw"@nl, "Construction"@en . 


```

!!! Tip "Querying on EventType"
	The type of construction event is stored in the value of a `sem:evenType` property, which is modelled as a blank node. To query for all events of type 'Verbouwing' or 'Construction', query on the `schema:name` value:

	=== "SPARQL"
	```SPARQL
	PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
	PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
	PREFIX schema: <http://schema.org/>
	PREFIX sem: <http://semanticweb.cs.vu.nl/2009/11/sem/>

	SELECT * WHERE {
		GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {
			?event a sem:Event;
				sem:eventType [ schema:name "Verbouwing" ] .
		}
	}

	```

Surprisingly, when a building is demolished, often the `sem:EventType` with `schema:name` 'Verbouwing' or 'Refurbishment' is used. 

**Example**

=== "Turtle (.ttl)"
```turtle
<http://www.cinemacontext.nl/id/constructionevent/id1114084252705-03>
	sem:hasPlace <http://www.cinemacontext.nl/id/place/id1114084252705> ;
	sem:eventType _:verbouwing ;
	sem:hasEarliestBeginTimeStamp "1940-05-14"^^xsd:date ;
	sem:hasLatestBeginTimeStamp "1940-05-14"^^xsd:date ;
	sem:hasEarliestEndTimeStamp "1940-05-14"^^xsd:date ;
	sem:hasLatestEndTimeStamp "1940-05-14"^^xsd:date ;
	schema:description "verwoest bij bombardement" ;
	a sem:Event .

_:verbouwing a sem:EventType ;
	schema:name "Verbouwing"@nl, "Refurbishment"@en . 
```





### Ratings

Films might be rated by a `schema:Rating`. These ratings might have a `schema:ratingValue` of either 'recommended' or 'not recommended', a `schema:text` and a `schema:ratingExplanation`. In the `schema:about` nodes, names and descriptions of the film as written in the rating report are mentioned.

| Property                      | Description                                                                                                                                                                           | Example                                                                                            |
| ----------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------- |
| rdf:type                      | A resource of this type is an instance of this class.                                                                                                                                 | schema:Rating, schema:CreativeWork                                                                 |
| schema:about                  | A rating is about a film. Modelled through the `schema:Role` class. The title of the film that was mentioned in the rating is given in the `schema:name` property of this blank node. | _:movie                                                                                            |
| schema:author                 | A rating is written by an organization.                                                                                                                                               | <<http://www.cinemacontext.nl/id/R001681>>                                                         |
| schema:dateCreated            | The date the rating was written or published.                                                                                                                                         | "1936-09-04"^^xsd:date                                                                             |
| sem:hasTimeStamp              | The date the rating was written or published, given as `xsd:date`.                                                                                                                    | "1953-09-15"^^xsd:date                                                                             |
| sem:hasEarliestBeginTimeStamp | The earliest possible date the rating was written or published, given as `xsd:date`.                                                                                                  | "1953-09-01"^^xsd:date                                                                             |
| sem:hasLatestEndTimeStamp     | The latest possible date the rating was written or published, given as `xsd:date`.                                                                                                    | "1953-09-30"^^xsd:date                                                                             |
| schema:identifier             | Some film screening ratings contain an identifier.                                                                                                                                    | "A3500"                                                                                            |
| schema:ratingExplanation      | Additional motivation for this rating.                                                                                                                                                | "Gevechten van dieren onderling."                                                                  |
| schema:ratingValue            | The rating's judgement. Either 'recommended' or 'not recommended'.                                                                                                                    | "recommended"                                                                                      |
| schema:text                   | Additional comments or age rating.                                                                                                                                                    | "alle leeftijden"                                                                                  |
| schema:sameAs                 | The rating can be present in another archive, such as the Nationaal Archief.                                                                                                          | <<https://www.nationaalarchief.nl/onderzoeken/index/nt00402/0b44c0b6-7cc1-102d-a57f-005056a23d00>> |

!!! Tip "Filmkeuringsrapporten 1928-1960 (National Archives of the Netherlands)"
	Some ratings entered in Cinema Context are coming from the archive of [_Filmkeuringsrapporten 1928-1960_](https://www.nationaalarchief.nl/onderzoeken/zoekhulpen/filmkeuringsrapporten-1928-1960) which is held by the National Archives of the Netherlands [Nationaal Archief]. The value in the `schema:sameAs` property of a rating points to the respective rating/report in this collection.


**Example**

=== "Turtle (.ttl)"
```turtle
<http://www.cinemacontext.nl/id/F020122>
	schema:contentRating <http://www.cinemacontext.nl/id/rating/Dossier10001> . 

<http://www.cinemacontext.nl/id/rating/Dossier10001>
	schema:identifier "V1308" ;
	sem:hasTimeStamp "1953-09-15"^^xsd:date ;
	sem:hasEarliestBeginTimeStamp "1953-09-15"^^xsd:date ;
	sem:hasLatestEndTimeStamp "1953-09-15"^^xsd:date ;
	schema:dateCreated "1953-09-15"^^xsd:date ;
	schema:author <http://www.cinemacontext.nl/id/R001681> ;
	schema:text "18 jaar" ;
	schema:ratingValue "not recommended" ;
	schema:ratingExplanation "te sensationeel voor onder de 18" ;
	schema:about [
		a schema:Role ;
		schema:name "Vrijdag, de dertiende" ;
		schema:description "r. Arthur Lubin, Boris Karloff" ;
		schema:about <http://www.cinemacontext.nl/id/F020122> ;
	] ;
	schema:about [
		a schema:Role ;
		schema:name "Black Friday" ;
		schema:about <http://www.cinemacontext.nl/id/F020122> ;
	] ;
	schema:sameAs <https://www.nationaalarchief.nl/onderzoeken/index/nt00402/0d3692d2-7cc1-102d-a57f-005056a23d00> ;
	a schema:Rating, schema:CreativeWork .
```
