# Hackalod 2020: The poster wall

On November’s Friday the 13th, a virtual Hackalod was held: a meeting of Linked (heritage) Data enthusiasts working together on team projects. Because of the covid crisis, the physical meeting that was originally planned could not happen. The purpose of this online meeting was to start working towards an actual face-to-face meeting in Spring 2021. Follow-up sessions are planned for every other Friday, where participants can share plans, ideas and progress reports on their team projects. People that are interested can still join, see for more information: https://www.netwerkdigitaalerfgoed.nl/events/hackalod-2020/ (note: all communication is in Dutch).

The team that worked on poster collections consisted of Menno den Engelse, Ivo Zandhuis and CREATE members Leon van Wissen and Thunnis van Oort. We were interested to investigate the poster collections that are available online via Dutch heritage institutions, what the status of the metadata was and if we could link the posters in various heritage collections to information on these posters (events, artists, locations, et cetera). We posted the first results of our investigation on this online poster wall: https://www.hicsuntleones.nl/plakmuur/ (information in Dutch). We looked at a number of specific collections:

* The International Institute for [Social History’s poster collection](https://www.hicsuntleones.nl/plakmuur/iisg/), where users can filter posters on specific topics, year or location.
* [Film posters from the EYE Film Institute](https://www.hicsuntleones.nl/plakmuur/eye/), as they have been made available via https://geheugen.delpher.nl/. These can also be filtered by year. 
* We have linked posters from the Rotterdam City Archives to the event location that the poster refers to, on the portal [Rotterdams publiek](https://rotterdamspubliek.nl/) (Rotterdam’s audiences). 
* We tagged the theatre posters in the [NAGO collection](https://www.hicsuntleones.nl/plakmuur/nago/) (Dutch graphic design archive). 

We outline the EYE film poster part below, as we closely tied it to the Cinema Context RDF data.

## EYE film posters
The data that we collected was transformed into RDF and is served on the [plakmuur website](https://www.hicsuntleones.nl/plakmuur). The EYE plakmuur queries the [CREATE SPARQL endpoint](https://data.create.humanities.uva.nl/) and uses data from two datasets: (1) The Cinema Context in RDF, and (2) The Afficheproject data from Het Geheugen van Nederland. Scripts for both harvesting and conversion can be found [here](https://gitlab.com/uvacreate/cinema-context/hackalod-2020). 

Where possible, a film poster has been manually linked to a Cinema Context film by specifying: `<poster> schema:about <movie>`. The film poster data from EYE is thereby automatically linked to the program data from Cinema Context, which in its turn is connected to other linked open datasets, such as Wikidata. 

On the plakmuur page (https://www.hicsuntleones.nl/plakmuur/eye/) you can see an overview of movies that were shown in cinemas in a particular year and week, in a particular city and/or cinema. Not every combination of these filters will render a result, due to limited availability of program data in Cinema Context. This will be expanded over time.

![](../images/hackalod2020.png)
Posters of films that were screened in 1927.

**Query (filters not specified in this example, this yields everything)**

=== "SPARQL"
```sparql
PREFIX sem: <http://semanticweb.cs.vu.nl/2009/11/sem/>
PREFIX schema: <http://schema.org/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
SELECT DISTINCT ?movie ?poster ?title ?depiction WHERE {
  GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {
    ?movie a schema:Movie ;
             schema:name ?title ;
             ^schema:workPresented ?screening .
    ?screening ^schema:subEvent ?programme .
                
    ?programme a schema:Event ;
               schema:location ?cinema ;
               sem:hasTimeStamp ?date .
    ?cinema schema:name ?cinemaName ;
               schema:location/schema:address/schema:addressLocality ?city .
  }
  
  GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/affiches/> {
    ?poster a schema:Poster ;
            schema:about ?movie ;
            schema:image ?depiction .
  }
```

Based on the selection of year, week, city, and cinema, these filters are inserted in the query:

**Filters**

=== "SPARQL"
```sparql
FILTER(year(?date) = 1910)
FILTER(bif:week(?date) = 1)  # special Virtuoso function
FILTER(REGEX(?city, "Amsterdam", 'i'))
FILTER(REGEX(?cinemaName, "Alhambra", "i"))
```

### More information
* The source code of this webpage is stored in Menno's repository: https://github.com/mmmenno/plakmuur/. 
* The harvest and conversion script, as well as the dataset can be found in: https://gitlab.com/uvacreate/cinema-context/hackalod-2020. 
* All poster data is included in Het Geheugen van Nederland: https://geheugen.delpher.nl/nl/geheugen/results?query=Eye+AND+afficheproject&page=1&maxperpage=36&coll=ngvn



