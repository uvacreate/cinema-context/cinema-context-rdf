# Getting started

## Accessing the data

The latest version of the Cinema Context RDF data is stored in the [CREATE Endpoint](https://data.create.humanities.uva.nl/) that also provides a YASGUI query interface. Queries can be directed at https://data.create.humanities.uva.nl/sparql.

!!! Warning "Cinema Context in a named graph (context)"
    You should specify a named graph in the CREATE endpoint, namely `<https://data.create.humanities.uva.nl/id/cinemacontext/>` when firing queries on the Cinema Context data in the CREATE endpoint. This is to separate the Cinema Context data from other datasets that also reside in the endpoint and to prevent interferance with similar or possibly conflicting statements from other datasets. 

    You can copy-paste this query as a starting point:
    ```SPARQL
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

    SELECT * WHERE {
    GRAPH <https://data.create.humanities.uva.nl/id/cinemacontext/> {
        ?sub ?pred ?obj .
      }
    } LIMIT 10
    ```

!!! Info "Dereferenceability (clickable and resolvable URIs)"
    In Linked Data it is good practice [to make your URIs clickable](https://www.w3.org/TR/cooluris/). This helps in understanding the identity of the entity you're describing as you can simply "follow your nose" in discovering more about it. As of 2020, some of the Cinema Context URIs are clickable and resolvable. These are the compact URIs ending with **B** (Cinemas), **F** (Films), **P** (Persons), **R** (Organizations), and **V** (Events), followed by a zero padded id. For now, you are redirected to the Cinema Context website.

## Obtaining the data

Alternatively, you can set up your own triple store to fire queries on your own (local) endpoint. Keep in mind that you still have to query the named graph `<https://data.create.humanities.uva.nl/id/cinemacontext/>` when you load this data (depending on your store).

### Latest version and version history

Check the [releases page](https://gitlab.com/uvacreate/cinema-context/cinema-context-rdf/-/releases) in the repository for all releases.

* [v1.0](https://gitlab.com/uvacreate/cinema-context/cinema-context-rdf/-/releases/v1.0) ([doi](https://doi.org/10.17026/dans-z64-mrvb)) was built on a production dump ([doi](https://doi.org/10.17026/dans-z9y-c5g6)) of the Cinema Context database of 2020-10-27. 

## Introduction into sparql

A basic introduction into writing sparql queries can be found [here](http://www.macs.hw.ac.uk/SWeL/tag/sparql-for-dummies/) or [here](https://programminghistorian.org/en/lessons/intro-to-linked-data). For a more elaborate introduction and guide, we recommend Bob DuCharme's [*Learning SPARQL*](http://learningsparql.com/). 
