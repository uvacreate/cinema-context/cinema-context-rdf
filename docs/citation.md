# License and citation 

## License

[![Creative Commons Attribution 4.0 International License](https://licensebuttons.net/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/)

The Cinema Context RDF data are licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

!!! Info "Getting the data"
    Check out the [getting started](../gettingstarted/#obtaining-the-data) section to obtain a copy of the RDF.
## Citation

Please cite the paper presented at SEMANTICS 2021:

* Leon van Wissen, Thunnis van Oort, Julia Noordegraaf, and Ivan Kisjes: Cinema Context as Linked Open Data: Converting an online Dutch film culture dataset to RDF. Joint Proceedings of the Semantics co-located events: Poster&Demo track and Workshop on Ontology-Driven Conceptual Modelling of Digital Twins co-located with Semantics 2021, Amsterdam, The Netherlands, September 6-9, 2021, CEUR-WS.org, online http://ceur-ws.org/Vol-2941/paper10.pdf

=== ".bib"
```bibtex

@inproceedings{cinemacontext:SemanticsP&Ds:2021,
  title    = {{Cinema Context as Linked Open Data: Converting an online Dutch film culture dataset to RDF}},
  author   = {Leon van Wissen and
              Thunnis van Oort and
              Julia Noordegraaf and
              Ivan Kisjes},
  url      = {http://ceur-ws.org/Vol-2941/paper10.pdf},
  crossref = {SemanticsP&Ds:2021}
}


@proceedings{SemanticsP&Ds:2021,
  editor = {Ilaria Tiddi and
            Maria Maleshkova and
            Tassilo Pellegrini and
            Victor de Boer},
  title  = {{Joint Proceedings of the Semantics co-located events: Poster&Demo track and Workshop on Ontology-Driven Conceptual Modelling of Digital Twins co-located with Semantics 2021 Amsterdam and Online, September 6-9, 2021.}},
  year   = {2021},
  month  = {September},
  series = {CEUR Workshop Proceedings},
  volume = {2941},
  url    = {http://ceur-ws.org/Vol-2941},
  urn    = {urn:nbn:de:0074-2941-5},
  issn   = {1613-0073},
  venue  = {Amsterdam, The Netherlands}
}

```


Or you can cite the dataset directly:

* Menno den Engelse, Leon van Wissen, Thunnis van Oort, and Julia Noordegraaf (2020): Cinema Context in RDF. Version 1.0. CREATE, University of Amsterdam. DANS. https://doi.org/10.17026/dans-z64-mrvb

=== ".bib"
```bibtex

@dataset{cinemacontextrdf2020v1,
  title        = {Cinema Context in RDF},
  author       = {Menno den Engelse and
                  Leon van Wissen and
                  Thunnis van Oort and
                  Julia Noordegraaf},
  organization = {CREATE, University of Amsterdam},
  month        = {October},
  year         = {2020},
  publisher    = {DANS},
  version      = {1.0},
  doi          = {10.17026/dans-z64-mrvb},
  url          = {https://doi.org/10.17026/dans-z64-mrvb}
}

```


Additionally, you may also cite the original data:

* Karel Dibbets (2018): Cinema Context. Film in Nederland vanaf 1896: een encyclopedie van de filmcultuur. Universiteit van Amsterdam. DANS. https://doi.org/10.17026/dans-z9y-c5g6


